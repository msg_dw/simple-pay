package me.xpyex.simplepay.command;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.velocitypowered.api.command.CommandMeta;
import com.velocitypowered.api.proxy.ProxyServer;
import me.xpyex.simplepay.command.sub.PayCommand;
import me.xpyex.simplepay.command.sub.ReloadCommand;
import me.xpyex.simplepay.config.SimplePayConfig.PayConfig;

/**
 * 命令管理器
 */
@Singleton
public class CommandManager {

    @Inject
    private ProxyServer server;

    @Inject
    private MainCommand mainCommand;

    @Inject
    private PayConfig payConfig;

    @Inject
    private Injector injector;

    /**
     * 注册主命令
     */
    public void registerMainCommand() {
        registerCommand(mainCommand);
        mainCommand.addSubCommand(PayCommand.class);
        mainCommand.addSubCommand(ReloadCommand.class);
        // addSubCommand(QueryCommand.class);
    }

    /**
     * 注册支付命令
     */
    public void registerPayCommand() {
        String command = payConfig.getCommand();
        if (!command.isEmpty()) {
            SimpleCommand simpleCommand = new SimpleCommand(command);
            injector.injectMembers(simpleCommand);
            registerCommand(simpleCommand);
        }
    }

    /**
     * 注册命令
     *
     * @param baseCommand 命令
     */
    public void registerCommand(BaseCommand baseCommand) {
        CommandMeta commandMeta = server.getCommandManager()
                .metaBuilder(baseCommand.getName())
                .aliases(baseCommand.getAliases())
                .build();
        server.getCommandManager().register(commandMeta, baseCommand);
    }

    /**
     * 获取主命令
     *
     * @return 主命令
     */
    public MainCommand getMainCommand() {
        return mainCommand;
    }
}
