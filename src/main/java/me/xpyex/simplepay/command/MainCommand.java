package me.xpyex.simplepay.command;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.velocitypowered.api.command.CommandSource;
import me.xpyex.simplepay.command.sub.PayCommand;
import me.xpyex.simplepay.command.sub.QueryCommand;
import me.xpyex.simplepay.command.sub.ReloadCommand;
import org.jetbrains.annotations.NotNull;

/**
 * 主命令
 */
public class MainCommand extends BaseCommand {

    @Inject
    public MainCommand() {
        super("simplepay", "spay");
        setPermission("simplepay.command");
    }

    @Override
    public void execute(@NotNull CommandSource source, @NotNull String label, @NotNull String... args) {
        if (args.length == 0) {
            l10n.sendMessage(source, "command.help");
            for (SubCommand subCommand : getSubCommands()) {
                if (subCommand.hasPermission(source)) {
                    l10n.sendMessage(source, "command." + subCommand.getName() + ".help");
                }
            }
        } else {
            super.execute(source, label, args);
        }
    }
}
