package me.xpyex.simplepay.command.sub;

import com.google.inject.Inject;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.scheduler.Scheduler;
import me.xpyex.simplepay.SimplePay;
import me.xpyex.simplepay.command.SubCommand;
import me.xpyex.simplepay.dao.OrderDao;
import me.xpyex.simplepay.domain.Order;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class QueryOrderCommand extends SubCommand {

    @Inject
    private SimplePay plugin;
    @Inject
    private Scheduler scheduler;
    @Inject
    private OrderDao orderDao;

    public QueryOrderCommand() {
        super("order");
        setPermission("simplepay.command.query.order");
    }

    @Override
    public void execute(@NotNull CommandSource source, @NotNull String label, @NotNull String... args) {
        if (args.length < 1) {
            l10n.sendMessage(source, "command.query.order.help");
            return;
        }

        long orderId;
        try {
            orderId = Long.parseLong(args[0]);
        } catch (NumberFormatException e) {
            l10n.sendMessage(source, "command.query.order.invalid-order", Map.of("%arg%", args[0]));
            return;
        }

        l10n.sendMessage(source, "command.query.order.start");
        scheduler.buildTask(plugin, () -> {
            Order order = orderDao.selectById(orderId);
            if (order == null) {
                l10n.sendMessage(source, "command.query.order.not-found", Map.of("%order%", orderId));
            } else {
                // TODO
                l10n.sendMessage(source, "command.query.order.success", Map.of(
                        "%order%", orderId,
                        "%server%", order.getServer(),
                        "%buyer%", order.getBuyer()
                ));
            }
        }).schedule();
    }

    @NotNull
    @Override
    public List<String> suggest(@NotNull CommandSource source, @NotNull String label, @NotNull String... args) {
        return Collections.emptyList();
    }
}
