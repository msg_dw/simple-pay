package me.xpyex.simplepay.command.sub;

import me.xpyex.simplepay.command.SubCommand;

/**
 * 查询命令
 */
public class QueryCommand extends SubCommand {

    public QueryCommand() {
        super("query");
        setPermission("simplepay.command.query");
        addSubCommand(QueryOrderCommand.class);
    }
}
