package me.xpyex.simplepay.command.sub;

import com.google.inject.Inject;
import com.velocitypowered.api.command.CommandSource;
import me.xpyex.simplepay.SimplePay;
import me.xpyex.simplepay.command.SubCommand;
import org.jetbrains.annotations.NotNull;

public class ReloadCommand extends SubCommand {

    @Inject
    private SimplePay simplePay;

    public ReloadCommand() {
        super("reload");
        setPermission("simplepay.command.reload");
    }

    @Override
    public void execute(@NotNull CommandSource source, @NotNull String label, @NotNull String... args) {
        simplePay.setup();
        l10n.sendMessage(source, "command.reload.finish");
    }
}
