package me.xpyex.simplepay.command.sub;

import com.google.inject.Inject;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import me.xpyex.simplepay.command.SubCommand;
import me.xpyex.simplepay.config.SimplePayConfig.PayConfig;
import me.xpyex.simplepay.pay.PayManager;
import me.xpyex.simplepay.pay.PayMethod;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 支付命令
 */
public class PayCommand extends SubCommand {

    @Inject
    private ProxyServer server;

    @Inject
    private PayConfig payConfig;

    @Inject
    private PayManager payManager;

    public PayCommand() {
        super("create");
        setPermission("simplepay.command.create");
    }

    @Override
    public void execute(@NotNull CommandSource source, @NotNull String label, @NotNull String... args) {
        if (args.length < 2) {
            // 参数不足
            l10n.sendMessage(source, "command.create.help");
            return;
        }

        Optional<PayMethod> payMethod = payManager.getPayMethod(args[0]);
        if (payMethod.isEmpty()) {
            // 支付方式不存在
            l10n.sendMessage(source, "command.create.invalid-pay-method", Map.of("%arg%", args[0]));
            return;
        }

        double money;
        if (payConfig.isUseDecimal()) {
            try {
                money = Double.parseDouble(args[1]);
            } catch (NumberFormatException e) {
                l10n.sendMessage(source, "command.create.invalid-money", Map.of("%arg%", args[1]));
                return;
            }
        } else {
            try {
                money = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                l10n.sendMessage(source, "command.create.invalid-money", Map.of("%arg%", args[1]));
                return;
            }
        }

        if (money < payConfig.getMinMoney()) {
            // 比最小金额还小
            l10n.sendMessage(source, "command.create.money-too-small", Map.of("%min%", payConfig.getMinMoney()));
            return;
        }

        if (money > payConfig.getMaxMoney()) {
            // 比最大额还大
            l10n.sendMessage(source, "command.create.money-too-much", Map.of("%max%", payConfig.getMaxMoney()));
            return;
        }

        Player player = null;
        // 为其它人开启支付 需要 other 权限
        if (args.length > 2 && source.hasPermission(getPermission() + ".other")) {
            Optional<Player> optionalPlayer = server.getPlayer(args[2]);
            if (optionalPlayer.isEmpty()) {
                // 玩家不在线
                l10n.sendMessage(source, "command.create.player-no-online", Map.of("%arg%", args[2]));
                return;
            }
            player = optionalPlayer.get();
        }

        // 为自己开启支付
        if (player == null) {
            if (!(source instanceof Player)) {
                // 不是玩家
                l10n.sendMessage(source, "command.create.no-player");
                return;
            }
            player = (Player) source;
        }

        // 发起支付
        payManager.initiatePay(player, payMethod.get(), money);
    }

    @Override
    public @NotNull List<String> suggest(@NotNull CommandSource source, @NotNull String label, @NotNull String... args) {
        // 第一个参数 支付方式
        if (args.length == 1) {
            return payManager.getPayMethodSuggestStream().filter(s ->
                    s.toLowerCase().startsWith(args[0].toLowerCase())
            ).collect(Collectors.toList());
        }
        // 第三个参数是一个可选的玩家名 但是需要权限
        else if (args.length == 3 && source.hasPermission(getPermission() + ".other")) {
            return server.getAllPlayers().stream().map(Player::getUsername).filter(s ->
                    s.toLowerCase().startsWith(args[0].toLowerCase())
            ).collect(Collectors.toList());
        }
        // 其它情况不需要补全
        return Collections.emptyList();
    }
}
