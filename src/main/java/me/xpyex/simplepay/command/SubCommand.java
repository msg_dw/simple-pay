package me.xpyex.simplepay.command;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.velocitypowered.api.command.CommandSource;
import me.xpyex.simplepay.config.Localization;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * 子命令
 */
public abstract class SubCommand {

    @Inject
    protected Injector injector;
    @Inject
    protected Localization l10n;

    /**
     * 命令的名称
     */
    private final String name;

    /**
     * 命令的别名
     */
    private final String[] aliases;

    /**
     * 命令所需权限
     */
    @Nullable
    private String permission;

    /**
     * 子命令
     */
    private final List<SubCommand> subCommands = new ArrayList<>();

    protected SubCommand(@NotNull String name, @NotNull String... aliases) {
        this.name = name;
        this.aliases = aliases;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public String[] getAliases() {
        return aliases;
    }

    @Nullable
    public String getPermission() {
        return permission;
    }

    public void setPermission(@Nullable String permission) {
        this.permission = permission;
    }

    /**
     * 判断是否有权限执行命令
     *
     * @param source 发送者
     * @return 是否拥有权限
     */
    public boolean hasPermission(@NotNull CommandSource source) {
        return getPermission() == null || source.hasPermission(getPermission());
    }

    public List<SubCommand> getSubCommands() {
        return subCommands;
    }

    /**
     * 通过一个名称获取子命令
     * 不存在则返回null
     *
     * @param alias 名称
     * @return 子命令
     */
    @Nullable
    public SubCommand getSubCommand(@NotNull String alias) {
        for (SubCommand subCommand : subCommands) {
            if (subCommand.name.equalsIgnoreCase(alias)) {
                return subCommand;
            }
            for (String a : subCommand.aliases) {
                if (a.equalsIgnoreCase(alias)) {
                    return subCommand;
                }
            }
        }
        return null;
    }

    /**
     * 添加一个子命令
     *
     * @param subCommandClass 子命令类
     * @throws IllegalArgumentException 已有重名命令
     */
    public void addSubCommand(@NotNull Class<? extends SubCommand> subCommandClass) {
        SubCommand subCommand = injector.getInstance(subCommandClass);
        if (getSubCommand(subCommand.name) != null) {
            throw new IllegalArgumentException("子命令名称冲突");
        }
        for (String alias : subCommand.aliases) {
            if (getSubCommand(alias) != null) {
                throw new IllegalArgumentException("子命令名称冲突");
            }
        }
        subCommands.add(subCommand);
    }

    public void execute(@NotNull CommandSource source, @NotNull String label, @NotNull String... args) {
        if (args.length > 0) {
            SubCommand subCommand = getSubCommand(args[0]);
            // 如果没有命令
            if (subCommand == null) {
                l10n.sendMessage(source, "command.not-exist", Map.of("%command%", args[0]));
                return;
            }
            // 如果没有权限
            if (!subCommand.hasPermission(source)) {
                l10n.sendMessage(source, "command.not-permission", Map.of("%command%", args[0], "%permission%", subCommand.getPermission()));
                return;
            }
            // 转发给下一级处理
            subCommand.execute(source, args[0], Arrays.copyOfRange(args, 1, args.length));
        }
    }

    @NotNull
    public List<String> suggest(@NotNull CommandSource source, @NotNull String label, @NotNull String... args) {
        // 子命令的补全传递
        if (args.length > 1) {
            SubCommand subCommand = getSubCommand(args[0]);
            // 子命令存在且有权限
            if (subCommand != null && subCommand.hasPermission(source)) {
                // 转发给下一级处理
                return subCommand.suggest(source, args[0], Arrays.copyOfRange(args, 1, args.length));
            }
        }
        // 补全自己可用的 子命令
        else {
            String oneArg = args.length == 0 ? "" : args[0].toLowerCase();
            List<String> suggest = new ArrayList<>();
            subCommands.stream().filter(subCommand -> subCommand.hasPermission(source)).forEach(subCommand -> {
                if (subCommand.name.toLowerCase().startsWith(oneArg)) {
                    suggest.add(subCommand.name);
                }
                for (String alias : subCommand.aliases) {
                    if (alias.toLowerCase().startsWith(oneArg)) {
                        suggest.add(alias);
                    }
                }
            });
            return suggest;
        }
        // 其他情况返回空集合
        return Collections.emptyList();
    }
}
