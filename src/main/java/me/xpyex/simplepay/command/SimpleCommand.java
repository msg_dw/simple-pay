package me.xpyex.simplepay.command;

import com.google.inject.Inject;
import com.velocitypowered.api.command.CommandSource;
import me.xpyex.simplepay.command.sub.PayCommand;
import me.xpyex.simplepay.config.SimplePayConfig;
import org.jetbrains.annotations.NotNull;

/**
 * 简单的命令
 */
public class SimpleCommand extends BaseCommand {

    @Inject
    private PayCommand payCommand;

    @Inject
    public SimpleCommand(String name) {
        super(name);
    }

    @Override
    public void execute(@NotNull CommandSource source, @NotNull String label, @NotNull String... args) {
        payCommand.execute(source, label, args);
    }
}
