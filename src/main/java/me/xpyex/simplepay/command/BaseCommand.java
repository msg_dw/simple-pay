package me.xpyex.simplepay.command;

import com.velocitypowered.api.command.SimpleCommand;

import java.util.List;

/**
 * 基础命令
 */
public class BaseCommand extends SubCommand implements SimpleCommand {

    public BaseCommand(String name, String... aliases) {
        super(name, aliases);
    }

    @Override
    public void execute(Invocation invocation) {
        execute(invocation.source(), invocation.alias(), invocation.arguments());
    }

    @Override
    public List<String> suggest(Invocation invocation) {
        return suggest(invocation.source(), invocation.alias(), invocation.arguments());
    }

    @Override
    public boolean hasPermission(Invocation invocation) {
        return hasPermission(invocation.source());
    }
}
