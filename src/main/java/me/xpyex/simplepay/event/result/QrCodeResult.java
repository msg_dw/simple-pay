package me.xpyex.simplepay.event.result;

import com.velocitypowered.api.event.ResultedEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.image.BufferedImage;
import java.util.Optional;

/**
 * 二维码的结果
 */
public class QrCodeResult implements ResultedEvent.Result {

    public static final QrCodeResult DEFAULT = new QrCodeResult(null);

    private final BufferedImage bufferedImage;

    private QrCodeResult(@Nullable BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    @Override
    public boolean isAllowed() {
        return true;
    }

    public Optional<BufferedImage> getBufferedImage() {
        return Optional.ofNullable(bufferedImage);
    }

    /**
     * 新的图像
     *
     * @param image 新的图像
     * @return 二维码的结果
     */
    public static QrCodeResult image(@NotNull BufferedImage image) {
        return new QrCodeResult(image);
    }
}
