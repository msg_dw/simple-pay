package me.xpyex.simplepay.event;

import com.velocitypowered.api.event.ResultedEvent;
import com.velocitypowered.api.event.ResultedEvent.GenericResult;
import lombok.Data;
import me.xpyex.simplepay.domain.Order;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 * 订单发货事件
 */
@Data
public class OrderShipEvent implements ResultedEvent<GenericResult>{

    /**
     * 事件结果
     */
    @NotNull
    private GenericResult result = GenericResult.allowed();

    /**
     * 玩家的UUID
     */
    @NotNull
    private final UUID playerUuid;

    /**
     * 玩家的名字
     */
    @NotNull
    private final String playerName;

    /**
     * 订单
     */
    @NotNull
    private final Order order;

    public OrderShipEvent(@NotNull UUID playerUuid, @NotNull String playerName, @NotNull Order order) {
        this.playerUuid = playerUuid;
        this.playerName = playerName;
        this.order = order;
    }
}
