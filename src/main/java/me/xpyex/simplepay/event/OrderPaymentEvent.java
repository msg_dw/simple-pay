package me.xpyex.simplepay.event;

import me.xpyex.simplepay.domain.Order;
import org.jetbrains.annotations.NotNull;

/**
 * 订单付款事件
 * 订单被支付时触发
 */
public class OrderPaymentEvent {

    /**
     * 订单号
     */
    private final Order order;

    public OrderPaymentEvent(@NotNull Order order) {
        this.order = order;
    }

    @NotNull
    public Order getOrder() {
        return order;
    }
}
