package me.xpyex.simplepay.event;

import com.velocitypowered.api.event.ResultedEvent;
import me.xpyex.simplepay.event.result.QrCodeResult;
import org.jetbrains.annotations.NotNull;

import java.awt.image.BufferedImage;

/**
 * 二维码的创建事件
 * 可以在这里对生成的二维码进行修改
 */
public class QrCodeCreateEvent implements ResultedEvent<QrCodeResult> {

    private QrCodeResult result = QrCodeResult.DEFAULT;

    private final String content;

    /**
     * 二维码图像
     */
    private final BufferedImage image;

    public QrCodeCreateEvent(@NotNull String content, @NotNull BufferedImage image) {
        this.content = content;
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    @NotNull
    public BufferedImage getImage() {
        return image;
    }

    @NotNull
    @Override
    public QrCodeResult getResult() {
        return result;
    }

    @Override
    public void setResult(@NotNull QrCodeResult result) {
        this.result = result;
    }


}
