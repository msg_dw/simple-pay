package me.xpyex.simplepay;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.velocitypowered.api.event.EventManager;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.event.proxy.ProxyShutdownEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;
import me.xpyex.simplepay.command.CommandManager;
import me.xpyex.simplepay.common.Constants;
import me.xpyex.simplepay.config.*;
import me.xpyex.simplepay.http.HttpServer;
import me.xpyex.simplepay.listener.PacketListener;
import me.xpyex.simplepay.listener.PlayerListener;
import me.xpyex.simplepay.listener.ShipListener;
import me.xpyex.simplepay.pay.PayManager;
import me.xpyex.simplepay.util.HttpUtil;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 插件入口
 */
@Plugin(id = SimplePay.PLUGIN_ID, name = SimplePay.PLUGIN_NAME, version = SimplePay.PLUGIN_VERSION)
public class SimplePay {

    public static final String PLUGIN_ID = "${project.id}";
    public static final String PLUGIN_NAME = "${project.name}";
    public static final String PLUGIN_VERSION = "${project.version}";

    @Inject
    private Logger logger;

    @Inject
    private ProxyServer server;

    @Inject
    private EventManager eventManager;

    @Inject
    private ConfigManager configManager;

    @Inject
    private CommandManager commandManager;

    @Inject
    private PayManager payManager;

    @Inject
    private Localization localization;

    @Inject
    private SimplePayConfig simplePayConfig;

    @Inject
    private HttpServer httpServer;

    @Inject
    private Injector injector;

    @Subscribe
    public void onInitialize(ProxyInitializeEvent event) {
        logger.info("插件正在启动...");
        // 注册监听器
        eventManager.register(this, injector.getInstance(PlayerListener.class));
        eventManager.register(this, injector.getInstance(PacketListener.class));
        eventManager.register(this, injector.getInstance(ShipListener.class));
        // 注册命令
        commandManager.registerMainCommand();
        // 一些基础配置
        setup();
        // 注册支付命令
        commandManager.registerPayCommand();

        // 检测更新
        if (simplePayConfig.getGlobalConfig().isCheckUpdate()) {
            server.getScheduler().buildTask(this, this::checkUpdate).schedule();
        }
    }

    @Subscribe
    public void onShutdown(ProxyShutdownEvent event) {
        logger.info("插件正在停止...");
        httpServer.close();
    }

    public void setup() {
        Configuration config = configManager.loadDefaultConfig();

        // 读取全局配置
        simplePayConfig.getGlobalConfig().setup(config.getConfiguration("global"));
        setupLanguage(simplePayConfig.getGlobalConfig().getLanguage());

        // 读取支付配置
        simplePayConfig.getPayConfig().setup(config.getConfiguration("pay"));

        // 读发货配置
        simplePayConfig.getShipConfig().setup(config.getConfiguration("ship"));

        // 读存储配置
        simplePayConfig.getStorageConfig().setup(config.getConfiguration("storage"));

        // 读服务器配置
        Configuration serverConfig = config.getConfiguration("server");
        int port = serverConfig.getInt("port", 30633);
        String notifyUrl = serverConfig.getString("notify");
        if (notifyUrl.isEmpty()) {
            try {
                // 如果没有输入就自动匹配以下本地的Ip
                String localhost = InetAddress.getLocalHost().getHostAddress();
                notifyUrl = String.format("http://%s:%d", localhost, port);
            } catch (UnknownHostException e) {
                throw new IllegalStateException(e);
            }
        } else {
            // 去除结尾的 /
            if (notifyUrl.charAt(notifyUrl.length() - 1) == '/') {
                notifyUrl = notifyUrl.substring(0, notifyUrl.length() - 1);
            }
        }
        httpServer.start(port);
        logger.info("服务器通知地址为: {}", notifyUrl);

        payManager.setup(config.getConfiguration("merchant"), notifyUrl);
    }

    public void setupLanguage(@NotNull String language) {
        String fileName = String.format("lang/%s.lang", language);
        File file = configManager.getFile(fileName);
        if (file.exists()) {
            localization.setLangConfig(Configuration.readYaml(file));
        } else {
            try {
                localization.setLangConfig(Configuration.readYaml(configManager.saveResource(fileName)));
            } catch (IllegalStateException e) {
                logger.error("读取语言配置文件失败!", e);
            }
        }
    }

    /**
     * 检查更新
     */
    public void checkUpdate() {
        try {
            logger.info("检测更新...");
            // 获取最近的5个发布
            String body = HttpUtil.get("https://gitee.com/api/v5/repos/msg_dw/simple-pay/releases?page=1&per_page=5&direction=desc");
            JsonArray array = Constants.GSON.fromJson(body, JsonArray.class);
            JsonObject latestVersion = array.get(0).getAsJsonObject();
            String name = latestVersion.get("name").getAsString();
            // 如果当前的版本不是最新版本
            if (!name.equals(PLUGIN_VERSION)) {
                logger.info("你当前运行的插件版本为: v{}", PLUGIN_VERSION);
                logger.info("找到一个更新的版本: v{}", name);
                String latestTag = latestVersion.get("tag_name").getAsString();
                logger.info("下载地址: https://gitee.com/msg_dw/simple-pay/releases/{}", latestTag);
                // 读取每个版本的更新内容
                boolean recent = false;
                for (JsonElement element : array) {
                    JsonObject version = element.getAsJsonObject();
                    String versionName = version.get("name").getAsString();
                    if (versionName.equals(PLUGIN_VERSION)) {
                        recent = true;
                        break;
                    }
                    String updateContent = version.get("body").getAsString();
                    logger.info("版本 {} 更新内容: ", versionName);
                    for (String line : updateContent.split(System.lineSeparator())) {
                        logger.info("\t{}", line);
                    }
                }
                if (!recent) {
                    logger.warn("你当前运行的版本过于久远, 无法找到更多的更新记录!");
                }
            } else {
                logger.info("你正在运行最新版的插件!");
            }
        } catch (Exception e) {
            logger.info("检测更新失败!");
        }
    }
}