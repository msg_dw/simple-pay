package me.xpyex.simplepay.http.handler;

import com.google.inject.Inject;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import me.xpyex.simplepay.http.HttpHandler;
import me.xpyex.simplepay.pay.PayManager;
import org.jetbrains.annotations.NotNull;

import javax.inject.Named;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

/**
 * 处理支付宝异步通知
 */
public class AliPayHandler implements HttpHandler {

    @Inject
    @Named("PublicKey")
    private String publicKey;

    @Inject
    private PayManager payManager;

    @Override
    public void handler(ChannelHandlerContext ctx, FullHttpRequest request) {
        String body = request.content().toString(StandardCharsets.UTF_8);
        String[] args = body.split("&");
        Map<String, String> params = new HashMap<>(args.length);
        for (String arg : args) {
            String[] split = arg.split("=");
            if (split.length == 2) {
                params.put(split[0], URLDecoder.decode(split[1], StandardCharsets.UTF_8));
            } else {
                throw new IllegalStateException("无效的参数");
            }
        }
        String sign = params.get("sign");
        String content = getSignCheckContent(params);
        // 签名验证 避免伪造请求
        boolean verify = verify(content, sign);
        if (verify) {
            payManager.markPay(params);
        }

        FullHttpResponse response = new DefaultFullHttpResponse(
                request.protocolVersion(),
                HttpResponseStatus.OK,
                Unpooled.copiedBuffer(verify ? "success" : "failure", StandardCharsets.UTF_8)
        );
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.TEXT_PLAIN);
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    /**
     * 获取需要验证的内容
     *
     * @param params 所有的参数
     * @return 需要验证的内容
     */
    @NotNull
    public String getSignCheckContent(@NotNull Map<String, String> params) {
        // 去掉这两个属性后 排序好 再拼接
        params.remove("sign");
        params.remove("sign_type");
        List<String> keys = new ArrayList<>(params.keySet());
        Collections.sort(keys);
        StringBuilder content = new StringBuilder();

        for (String key : keys) {
            content.append(key).append("=").append(params.get(key)).append("&");
        }
        if (content.length() > 0) {
            content.deleteCharAt(content.length() - 1);
        }

        return content.toString();
    }

    /**
     * 验证
     *
     * @param content 需要验证的内容
     * @param sign    签名
     * @return 是否通过
     */
    public boolean verify(@NotNull String content, @NotNull String sign) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] encodedKey = Base64.getDecoder().decode(publicKey.getBytes());
            PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));
            Signature signature = Signature.getInstance("SHA256WithRSA");
            signature.initVerify(pubKey);
            signature.update(content.getBytes(StandardCharsets.UTF_8));
            return signature.verify(Base64.getDecoder().decode(sign.getBytes()));
        } catch (Exception e) {
            throw new IllegalStateException("签名验证出错", e);
        }
    }
}
