package me.xpyex.simplepay.http.handler;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 处理中国银联支付的异步通知
 */
public class UnionPayHandler extends TencentPayHandler {

    @Override
    public void handler(ChannelHandlerContext ctx, FullHttpRequest request) {
        String body = request.content().toString(StandardCharsets.UTF_8);
        Map<String, String> params = xmlToMap(body);

        boolean verify = false;
        if ("0".equals(params.get("status")) && "0".equals(params.get("result_code")) && verify(params)) {
            verify = true;
            payManager.markPay(params);
        }
        FullHttpResponse response = new DefaultFullHttpResponse(
                request.protocolVersion(),
                HttpResponseStatus.OK,
                Unpooled.copiedBuffer(verify ? "success" : "fail", StandardCharsets.UTF_8)
        );
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.TEXT_PLAIN);
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }
}
