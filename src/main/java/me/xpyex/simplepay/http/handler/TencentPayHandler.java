package me.xpyex.simplepay.http.handler;

import com.google.inject.Inject;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import me.xpyex.simplepay.http.HttpHandler;
import me.xpyex.simplepay.pay.PayManager;
import me.xpyex.simplepay.pay.method.TencentPayMethod;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.inject.Named;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * 处理腾讯系支付的异步通知
 */
public class TencentPayHandler implements HttpHandler {

    @Inject
    @Named("MerchantKey")
    protected String merchantKey;

    @Inject
    protected PayManager payManager;

    @Override
    public void handler(ChannelHandlerContext ctx, FullHttpRequest request) {
        String body = request.content().toString(StandardCharsets.UTF_8);
        Map<String, String> params = xmlToMap(body);

        if (verify(params)) {
            if ("SUCCESS".equals(params.get("trade_state")) || "SUCCESS".equals(params.get("return_code"))) {
                payManager.markPay(params);
                FullHttpResponse response = new DefaultFullHttpResponse(
                        request.protocolVersion(),
                        HttpResponseStatus.OK,
                        Unpooled.copiedBuffer("<xml>" +
                                "<return_code>SUCCESS</return_code>" +
                                "<return_msg>OK</return_msg>" +
                                "</xml>", StandardCharsets.UTF_8)
                );
                response.headers().set(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.TEXT_PLAIN);
                ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
            }
        }
    }

    /**
     * 将xml字符串转为Map
     *
     * @param xml xml
     * @return map
     */
    @NotNull
    public Map<String, String> xmlToMap(@NotNull String xml) {
        try {
            // 其实我想用 正则解析的... xml不好用
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
            dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            dbf.setXIncludeAware(false);
            dbf.setExpandEntityReferences(false);
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            Document document = documentBuilder.parse(new ByteArrayInputStream(xml.getBytes()));
            Element element = document.getDocumentElement();
            NodeList childNodes = element.getChildNodes();
            Map<String, String> map = new HashMap<>(childNodes.getLength());
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node node = childNodes.item(i);
                map.put(node.getNodeName(), node.getTextContent());
            }
            map.remove("#text");
            return map;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 验证
     *
     * @param params 参数
     * @return 验证成功
     */
    public boolean verify(@NotNull Map<String, String> params) {
        String sign = params.remove("sign");
        List<String> keys = new ArrayList<>(params.keySet());
        Collections.sort(keys);
        StringBuilder content = new StringBuilder();

        for (String key : keys) {
            content.append(key).append("=").append(params.get(key)).append("&");
        }
        if (content.length() > 0) {
            content.deleteCharAt(content.length() - 1);
            content.append("&key=").append(merchantKey);
        }
        String localSign = TencentPayMethod.md5(content.toString());
        return sign.equals(localSign);
    }
}
