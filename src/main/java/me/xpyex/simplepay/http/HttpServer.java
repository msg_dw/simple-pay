package me.xpyex.simplepay.http;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.*;
import me.xpyex.simplepay.config.ConfigManager;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * 使用netty内建HTTP服务器
 */
@Singleton
public class HttpServer {

    @Inject
    private Logger logger;

    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;

    private FullHttpResponse defaultPage;

    private final Map<String, HttpHandler> handlers = new HashMap<>();

    @Inject
    public HttpServer(ConfigManager configManager) {
        setDefaultPage(configManager.readResourceString("index.html"));
    }

    /**
     * 启动服务端
     */
    public void start(int port) {
        close();
        bossGroup = new NioEventLoopGroup(1);
        workerGroup = new NioEventLoopGroup();
        ServerBootstrap bootstrap = new ServerBootstrap()
                .option(ChannelOption.SO_BACKLOG, 1024)
                .group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<>() {
                    @Override
                    protected void initChannel(@NotNull Channel ch) {
                        ch.pipeline()
                                .addLast(new HttpServerCodec())
                                .addLast(new HttpObjectAggregator(65535))
                                .addLast(new SimpleChannelInboundHandler<FullHttpRequest>() {

                                    @Override
                                    public void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request) {
                                        logger.debug("req method: {}, req uri: {}", request.method(), request.uri());
                                        // 干掉图标
                                        if ("/favicon.ico".equals(request.uri())) {
                                            return;
                                        }
                                        // GET请求 全部发送默认页
                                        if (request.method() == HttpMethod.GET) {
                                            ctx.writeAndFlush(defaultPage.copy()).addListener(ChannelFutureListener.CLOSE);
                                            return;
                                        }
                                        // POST请求
                                        if (request.method() == HttpMethod.POST) {
                                            HttpHandler handler = handlers.get(request.uri());
                                            if (handler != null) {
                                                try {
                                                    handler.handler(ctx, request);
                                                } catch (IllegalStateException e) {
                                                    logger.debug("invalid request uri:{}, {}", request.uri(), e.getMessage());
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void channelReadComplete(ChannelHandlerContext ctx) {
                                        ctx.flush();
                                    }
                                });
                    }
                });

        try {
            bootstrap.bind(port).sync();
            logger.info("在 {} 端口上启动HTTP服务器.", port);
        } catch (InterruptedException e) {
            logger.error("HTTP服务器启动失败", e);
        }
    }

    /**
     * 关闭HTTP服务器
     */
    public void close() {
        if (bossGroup == null || workerGroup == null) {
            return;
        }
        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
    }

    /**
     * 设置默认页
     *
     * @param html 内容
     */
    public void setDefaultPage(@NotNull String html) {
        FullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1,
                HttpResponseStatus.OK,
                Unpooled.copiedBuffer(html, StandardCharsets.UTF_8)
        );
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/html; charset=UTF-8");
        defaultPage = response;
    }

    /**
     * 添加处理程序
     * 重名的URI将被覆盖
     *
     * @param uri     URI
     * @param handler 处理程序
     */
    public void addHandler(@NotNull String uri, @NotNull HttpHandler handler) {
        handlers.put(uri, handler);
    }
}
