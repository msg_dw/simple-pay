package me.xpyex.simplepay.http;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

@FunctionalInterface
public interface HttpHandler {

    /**
     * 处理HTTP请求
     *
     * @param ctx     ctx
     * @param request HTTP请求
     */
    void handler(ChannelHandlerContext ctx, FullHttpRequest request);
}
