package me.xpyex.simplepay.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.Data;
import lombok.Getter;

import java.util.List;

/**
 * 插件配置
 */
@Getter
@Singleton
public class SimplePayConfig {

    @Inject
    private GlobalConfig globalConfig;

    /**
     * 支付配置
     */
    @Inject
    private PayConfig payConfig;

    /**
     * 发货设置
     */
    @Inject
    private ShipConfig shipConfig;

    /**
     * 存储配置
     */
    @Inject
    private StorageConfig storageConfig;

    @Data
    @Singleton
    public static class GlobalConfig {

        /**
         * 语言
         */
        private String language = "zh_cn";

        /**
         * 检测更新
         */
        private boolean checkUpdate = true;

        /**
         * 配置
         *
         * @param payConfig 支付配置
         */
        public void setup(Configuration payConfig) {
            language = payConfig.getString("language", "zh_cn");
            checkUpdate = payConfig.getBoolean("check-update", true);
        }
    }

    @Data
    @Singleton
    public static class PayConfig {

        /**
         * 支付命令
         */
        private String command = "";

        /**
         * 低头模式
         */
        private boolean headDown = true;

        /**
         * 是否允许使用小数充值
         */
        private boolean useDecimal = true;

        /**
         * 支付的最小金额
         */
        private int minMoney = 1;

        /**
         * 支付的最大金额
         */
        private int maxMoney = 200;

        /**
         * 配置
         *
         * @param payConfig 支付配置
         */
        public void setup(Configuration payConfig) {
            command = payConfig.getString("command");
            headDown = payConfig.getBoolean("head-down");
            useDecimal = payConfig.getBoolean("use-decimal");
            minMoney = Math.max(1, payConfig.getInt("min-money"));
            maxMoney = payConfig.getInt("max-money", 200);
        }
    }

    @Data
    @Singleton
    public static class ShipConfig {
        /**
         * 是否启用
         */
        private boolean enable;

        /**
         * 需要玩家在线
         */
        private boolean needOnline;

        /**
         * 支付后立即发货
         */
        private boolean onPay = true;

        /**
         * 加入时进行发货
         */
        private boolean onJoin;

        /**
         * 切换服务器时进行发货
         */
        private boolean onServer;

        /**
         * 服务器连接完成
         */
        private boolean onServerFinish;

        /**
         * 发货时执行的命令
         */
        private List<String> commands;

        /**
         * 配置
         *
         * @param shipConfig 发货配置
         */
        public void setup(Configuration shipConfig) {
            enable = shipConfig.getBoolean("enable");
            needOnline = shipConfig.getBoolean("need-online");
            onPay = shipConfig.getBoolean("on-pay", true);
            onJoin = shipConfig.getBoolean("on-join");
            onServer = shipConfig.getBoolean("on-server");
            onServerFinish = shipConfig.getBoolean("on-server-finish");
            commands = shipConfig.getStringList("commands");
        }
    }

    @Data
    @Singleton
    public static class StorageConfig {

        /**
         * 驱动类
         */
        private String driverClassName;

        /**
         * 数据库地址
         */
        private String url;

        /**
         * 用户名
         */
        private String username;

        /**
         * 密码
         */
        private String password;

        /**
         * 配置
         *
         * @param storageConfig 存储配置
         */
        public void setup(Configuration storageConfig) {
            driverClassName = storageConfig.getString("driver-class-name");
            try {
                Class.forName(driverClassName);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("找不到数据库驱动!", e);
            }
            url = storageConfig.getString("url");
            username = storageConfig.getStringOrNull("username");
            password = storageConfig.getStringOrNull("password");
        }
    }
}
