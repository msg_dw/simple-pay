package me.xpyex.simplepay.config;

import com.google.inject.Inject;
import com.velocitypowered.api.plugin.annotation.DataDirectory;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * 配置文件管理器
 */
public class ConfigManager {

    private static final String CONFIG_FILE_NAME = "config.yml";

    @Inject
    @DataDirectory
    private Path folderPath;

    /**
     * 加载默认的配置文件 config.yml
     *
     * @return 配置
     */
    public Configuration loadDefaultConfig() {
        return Configuration.readYaml(saveResource(CONFIG_FILE_NAME));
    }

    /**
     * 保存资源到插件文件夹
     *
     * @param resource 资源
     */
    public File saveResource(String resource) {
        return saveResource(resource, false);
    }

    @NotNull
    public File saveResource(@NotNull String resource, boolean overwrite) {
        File file = getFile(resource);
        if (!overwrite && file.exists()) {
            return file;
        }
        try (
                InputStream in = getResource(resource);
                OutputStream out = new FileOutputStream(makeFile(file))
        ) {
            int len;
            byte[] buffer = new byte[4096];
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
            return file;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 读资源文件里面的字符串内容
     *
     * @return 字符串内容
     */
    @NotNull
    public String readResourceString(@NotNull String resource) {
        try {
            return readInputStreamString(getResource(resource));
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 读资源文件里面的字符串内容
     *
     * @return 字符串内容
     */
    @NotNull
    public String readInputStreamString(@NotNull InputStream inputStream) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line).append(System.lineSeparator());
            }
            return builder.toString();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 从Jar包内获取资源的流
     *
     * @param resource 资源
     * @return 输入流
     * @throws FileNotFoundException 资源文件不存在
     */
    @NotNull
    public InputStream getResource(String resource) throws FileNotFoundException {
        ClassLoader classLoader = this.getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(resource);
        if (inputStream == null) {
            throw new FileNotFoundException("资源文件不存在");
        }
        return inputStream;
    }

    /**
     * 获取插件配置文件夹中的一个文件
     *
     * @param fileName 文件名
     * @return 文件
     */
    @NotNull
    public File getFile(@NotNull String fileName) {
        return new File(folderPath.toFile(), fileName);
    }

    /**
     * 制作文件
     * 如果文件不存在会创建文件
     * 如果存在会直接返回
     *
     * @param file 文件
     * @return 文件
     */
    @NotNull
    public File makeFile(@NotNull File file) {
        if (file.exists()) {
            return file;
        }
        File parentFile = file.getParentFile();
        if (parentFile.exists() && parentFile.isFile()) {
            throw new IllegalStateException("文件路径无法创建");
        }
        try {
            parentFile.mkdirs();
            file.createNewFile();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return file;
    }

}
