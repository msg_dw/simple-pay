package me.xpyex.simplepay.config;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * 配置文件
 * 简单的读取配置文件
 * 不提供 key1.key2.key3 这种方式 懒得兼容
 * 不提供保存 因为我不需要
 */
public class Configuration implements Iterable<Configuration> {

    public static final Configuration EMPTY = new Configuration(Map.of());

    @NotNull
    private final Map<String, Object> values;
    private final String key;

    private Configuration(@NotNull Map<String, Object> values) {
        this(values, null);
    }

    private Configuration(@NotNull Map<String, Object> values, String key) {
        this.values = values;
        this.key = key;
    }

    /**
     * 返回自己的节点
     * 如果是根节点则为空
     *
     * @return 节点
     */
    @Nullable
    public String getKey() {
        return key;
    }

    /**
     * 判断节点是否存在
     *
     * @param key 节点
     * @return 是否存在
     */
    public boolean contains(@NotNull String key) {
        try {
            return values.containsKey(key);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 设置配置文件中Key节点对应的数据
     *
     * @param key    节点
     * @param object 数据
     */
    public void set(@NotNull String key, @NotNull Object object) {
        values.put(key, object);
    }

    /**
     * 移除配置文件中Key节点对应的数据
     *
     * @param key 节点
     * @return 被移除的数据
     */
    @Nullable
    public Object remove(@NotNull String key) {
        return values.remove(key);
    }

    /**
     * 获取配置文件中Key节点对应的数据
     *
     * @param key 节点
     * @param <T> 数据类型
     * @return 数据
     */
    @Nullable
    @SuppressWarnings("unchecked")
    public <T> T get(@NotNull String key) {
        return (T) values.get(key);
    }

    /**
     * 获取配置文件中Key节点对应的数据
     *
     * @param key 节点
     * @param def 默认值 如果你需要传入null 请直接调用 get方法
     * @param <T> 数据类型
     * @return 数据
     */
    @NotNull
    @SuppressWarnings("unchecked")
    public <T> T get(@NotNull String key, @NotNull T def) {
        Object result = values.get(key);
        return result == null ? def : (T) result;
    }

    /**
     * 获取Object不进行类型转换
     *
     * @param key 节点
     * @return 数据
     */
    @Nullable
    public Object getObject(@NotNull String key) {
        return get(key);
    }

    /**
     * 获取Object不进行类型转换
     *
     * @param key 节点
     * @param def 默认值
     * @return 数据
     */
    @NotNull
    public Object getObject(@NotNull String key, @NotNull Object def) {
        return get(key, def);
    }

    /**
     * 获取字符串
     *
     * @param key 节点
     * @return 字符串
     */
    @NotNull
    public String getString(@NotNull String key) {
        return getString(key, "");
    }

    /**
     * 获取字符串
     *
     * @param key 节点
     * @param def 默认值
     * @return 字符串
     */
    @NotNull
    public String getString(@NotNull String key, @NotNull String def) {
        Object o = get(key);
        return o == null ? def : o.toString();
    }

    /**
     * 获取字符串
     * 如果不存在将返回null
     *
     * @param key 节点
     * @return 字符串
     */
    @Nullable
    public String getStringOrNull(@NotNull String key) {
        Object o = get(key);
        return o == null ? null : o.toString();
    }

    /**
     * 获取整形
     *
     * @param key 节点
     * @return 整形
     */
    public int getInt(@NotNull String key) {
        return getInt(key, 0);
    }

    /**
     * 获取整形
     *
     * @param key 节点
     * @param def 默认值
     * @return 整形
     */
    public int getInt(@NotNull String key, int def) {
        Number number = get(key);
        return number == null ? def : number.intValue();
    }

    /**
     * 获取布尔值
     *
     * @param key 节点
     * @return 布尔值
     */
    public boolean getBoolean(@NotNull String key) {
        return get(key, false);
    }

    /**
     * 获取布尔值
     *
     * @param key 节点
     * @param def 默认值
     * @return 布尔值
     */
    public boolean getBoolean(@NotNull String key, boolean def) {
        Boolean bool = get(key);
        return bool == null ? def : bool;
    }

    /**
     * 获取一个字符串集合
     *
     * @param key 节点
     * @return 字符串集合
     */
    @NotNull
    public List<String> getStringList(@NotNull String key) {
        List<String> list = get(key, new ArrayList<>());
        if (list.isEmpty()) {
            values.put(key, list);
        }
        return list;
    }

    /**
     * 获取子配置
     *
     * @param key 节点
     * @return 子配置
     */
    @NotNull
    public Configuration getConfiguration(@NotNull String key) {
        HashMap<String, Object> subValues = get(key, new LinkedHashMap<>());
        if (subValues.isEmpty()) {
            values.put(key, subValues);
        }
        return new Configuration(subValues, key);
    }

    @NotNull
    @Override
    public Iterator<Configuration> iterator() {
        return values.keySet().stream().map(this::getConfiguration).iterator();
    }

    /**
     * 使用Yaml格式读取文件
     *
     * @param file 文件
     * @return 配置
     */
    @NotNull
    public static Configuration readYaml(@NotNull File file) {
        try (Reader reader = new FileReader(file, StandardCharsets.UTF_8)) {
            return new Configuration(new Yaml().load(reader));
        } catch (IOException e) {
            throw new IllegalStateException("配置文件读取失败!", e);
        }
    }
}
