package me.xpyex.simplepay.config;

import com.google.inject.Singleton;
import com.velocitypowered.api.command.CommandSource;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Map.Entry;

/**
 * 本地化配置
 */
@Singleton
public class Localization {

    /**
     * 语言配置
     */
    private Configuration langConfig = Configuration.EMPTY;

    @NotNull
    public Configuration getLangConfig() {
        return langConfig;
    }

    public void setLangConfig(@NotNull Configuration langConfig) {
        this.langConfig = langConfig;
    }

    /**
     * 获取本地化文本
     *
     * @param node 节点
     * @return 本地化文本
     */
    public String get(String node) {
        if (langConfig.contains(node)) {
            return langConfig.getString(node);
        }
        return node;
    }

    /**
     * 解析本地化文本
     *
     * @param node         节点
     * @param replaceTable 替换表
     * @return 本地化文本
     */
    public String parse(String node, Map<String, Object> replaceTable) {
        if (langConfig.contains(node)) {
            String string = langConfig.getString(node);
            for (Entry<String, Object> entry : replaceTable.entrySet()) {
                string = string.replace(entry.getKey(), entry.getValue().toString());
            }
            return string;
        }
        return node;
    }

    /**
     * 发送本地化文本
     *
     * @param source 发送对象
     * @param node   节点
     */
    public void sendMessage(CommandSource source, String node) {
        String message = get(node);
        if (!message.isEmpty()) {
            source.sendMessage(LegacyComponentSerializer.legacyAmpersand().deserialize(message));
        }
    }

    /**
     * 发送本地化文本
     *
     * @param source 发送对象
     * @param node   节点
     */
    public void sendMessage(CommandSource source, String node, Map<String, Object> replaceTable) {
        String message = parse(node, replaceTable);
        if (!message.isEmpty()) {
            source.sendMessage(LegacyComponentSerializer.legacyAmpersand().deserialize(message));
        }
    }
}
