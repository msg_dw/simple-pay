package me.xpyex.simplepay.domain;

import lombok.Data;

import java.sql.Timestamp;

/**
 * 订单号
 */
@Data
public class Order {

    /**
     * 订单号
     */
    private long id;

    /**
     * 服务器名称
     */
    private String server;

    /**
     * 买家
     */
    private String buyer;

    /**
     * 订单标题
     */
    private String subject;

    /**
     * 订单金额
     */
    private double totalFee;

    /**
     * 订单状态
     */
    private int status;

    /**
     * 发货状态
     */
    private int ship;

    /**
     * 创建事件
     */
    private Timestamp createTime;

    // 下方只有付款后才会存在

    /**
     * 支付事件
     */
    private Timestamp payTime;

    /**
     * 支付信息
     */
    private String payInfo;
}
