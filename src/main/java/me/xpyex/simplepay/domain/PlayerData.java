package me.xpyex.simplepay.domain;

import lombok.Data;

import java.util.UUID;

/**
 * 玩家数据
 */
@Data
public class PlayerData {

    /**
     * 玩家的UUID
     */
    private UUID uuid;

    /**
     * 玩家的名字
     */
    private String name;

    /**
     * 玩家的充值金额
     */
    private double amount;
}
