package me.xpyex.simplepay.util;

import com.velocitypowered.api.network.ProtocolVersion;
import com.velocitypowered.api.proxy.Player;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.Channel;
import io.netty.handler.codec.CorruptedFrameException;
import io.netty.handler.codec.EncoderException;
import net.kyori.adventure.nbt.BinaryTag;
import net.kyori.adventure.nbt.BinaryTagType;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;

import java.io.IOException;
import java.lang.reflect.Method;

public class ProtocolUtil {

    private static final Class<?> PROTOCOL_UTILS_CLASS = clazz("com.velocitypowered.proxy.protocol.ProtocolUtils");
    private static final Class<?> MINECRAFT_CONNECTION_CLASS = clazz("com.velocitypowered.proxy.connection.MinecraftConnection");
    private static final Class<?> CONNECTED_PLAYER_CLASS = clazz("com.velocitypowered.proxy.connection.client.ConnectedPlayer");
    private static final Method $1 = method(PROTOCOL_UTILS_CLASS, "getJsonChatSerializer", ProtocolVersion.class);
    private static final Method $2 = method(MINECRAFT_CONNECTION_CLASS, "getProtocolVersion");
    private static final Method $3 = method(MINECRAFT_CONNECTION_CLASS, "getChannel");
    private static final Method $4 = method(MINECRAFT_CONNECTION_CLASS, "write", Object.class);
    private static final Method $5 = method(CONNECTED_PLAYER_CLASS, "getConnection");
    private static final Method $6 = method(CONNECTED_PLAYER_CLASS, "getConnectedServer");

    private static Class<?> clazz(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static Method method(Class<?> clazz, String methodName, Class<?>... parameterTypes) {
        try {
            return clazz.getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public static GsonComponentSerializer getJsonChatSerializer(ProtocolVersion version) {
        String gson = GsonComponentSerializer.gson().serialize(Component.text("xxx"));
        try {
            return (GsonComponentSerializer) $1.invoke(null, version);
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException(e);
        }
    }

    public static boolean isConnectedPlayer(Player player) {
        return CONNECTED_PLAYER_CLASS.isInstance(player);
    }

    public static ProtocolVersion getProtocolVersion(Object instance) {
        try {
            return (ProtocolVersion) $2.invoke(instance);
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Object getConnection(Player player) {
        try {
            return $5.invoke(player);
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Channel getChannel(Player player) {
        try {
            return (Channel) $3.invoke(getConnection(player));
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Object getServerConnection(Player player) {
        try {
            Object connectedServer = $6.invoke(player);
            if (connectedServer != null) {
                Method method = connectedServer.getClass().getMethod("getConnection");
                return method.invoke(connectedServer);
            }
            return null;
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void connectionWrite(Object instance, ByteBuf buf) {
        try {
            $4.invoke(instance, buf);
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Reads a Minecraft-style VarInt from the specified {@code buf}.
     *
     * @param buf the buffer to read from
     * @return the decoded VarInt
     */
    public static int readVarInt(ByteBuf buf) {
        int read = readVarIntSafely(buf);
        if (read == Integer.MIN_VALUE) {
            throw new CorruptedFrameException("Bad VarInt decoded");
        }
        return read;
    }

    /**
     * Reads a Minecraft-style VarInt from the specified {@code buf}. The difference between this
     * method and {@link #readVarInt(ByteBuf)} is that this function returns a sentinel value if the
     * varint is invalid.
     *
     * @param buf the buffer to read from
     * @return the decoded VarInt, or {@code Integer.MIN_VALUE} if the varint is invalid
     */
    public static int readVarIntSafely(ByteBuf buf) {
        int i = 0;
        int maxRead = Math.min(5, buf.readableBytes());
        for (int j = 0; j < maxRead; j++) {
            int k = buf.readByte();
            i |= (k & 0x7F) << j * 7;
            if ((k & 0x80) != 128) {
                return i;
            }
        }
        return Integer.MIN_VALUE;
    }

    /**
     * Writes a Minecraft-style VarInt to the specified {@code buf}.
     *
     * @param buf   the buffer to read from
     * @param value the integer to write
     */
    public static void writeVarInt(ByteBuf buf, int value) {
        // Peel the one and two byte count cases explicitly as they are the most common VarInt sizes
        // that the proxy will write, to improve inlining.
        if ((value & (0xFFFFFFFF << 7)) == 0) {
            buf.writeByte(value);
        } else if ((value & (0xFFFFFFFF << 14)) == 0) {
            int w = (value & 0x7F | 0x80) << 8 | (value >>> 7);
            buf.writeShort(w);
        } else {
            writeVarIntFull(buf, value);
        }
    }

    private static void writeVarIntFull(ByteBuf buf, int value) {
        // See https://steinborn.me/posts/performance/how-fast-can-you-write-a-varint/
        if ((value & (0xFFFFFFFF << 7)) == 0) {
            buf.writeByte(value);
        } else if ((value & (0xFFFFFFFF << 14)) == 0) {
            int w = (value & 0x7F | 0x80) << 8 | (value >>> 7);
            buf.writeShort(w);
        } else if ((value & (0xFFFFFFFF << 21)) == 0) {
            int w = (value & 0x7F | 0x80) << 16 | ((value >>> 7) & 0x7F | 0x80) << 8 | (value >>> 14);
            buf.writeMedium(w);
        } else if ((value & (0xFFFFFFFF << 28)) == 0) {
            int w = (value & 0x7F | 0x80) << 24 | (((value >>> 7) & 0x7F | 0x80) << 16)
                    | ((value >>> 14) & 0x7F | 0x80) << 8 | (value >>> 21);
            buf.writeInt(w);
        } else {
            int w = (value & 0x7F | 0x80) << 24 | ((value >>> 7) & 0x7F | 0x80) << 16
                    | ((value >>> 14) & 0x7F | 0x80) << 8 | ((value >>> 21) & 0x7F | 0x80);
            buf.writeInt(w);
            buf.writeByte(value >>> 28);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends BinaryTag> void writeBinaryTag(ByteBuf buf, T tag, ProtocolVersion version) {
        BinaryTagType<T> type = (BinaryTagType<T>) tag.type();
        buf.writeByte(type.id());
        try {
            if (ProtocolUtil.compare(version, ProtocolVersions.MINECRAFT_1_20_2) < 0) {
                buf.writeShort(0);
            }
            type.write(tag, new ByteBufOutputStream(buf));
        } catch (IOException e) {
            throw new EncoderException("Unable to encode BinaryTag");
        }
    }

    public static void writeByteArray(ByteBuf buf, byte[] array) {
        writeVarInt(buf, array.length);
        buf.writeBytes(array);
    }

    public static int compare(ProtocolVersion protocolVersion, ProtocolVersions protocolVersions) {
        return Integer.compare(protocolVersion.getProtocol(), protocolVersions.getProtocol());
    }
}
