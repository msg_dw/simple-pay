package me.xpyex.simplepay.util;

import com.velocitypowered.api.network.ProtocolVersion;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 这个工具类将图像的rpg色转换到mc中最相似的地图色
 * 地图色使用一个无符号byte存储, 它的有效范围是0-255
 * 参考: https://wiki.biligame.com/mc/地图物品格式
 */
public class MapColor {

    /**
     * 地图尺寸
     */
    private static final int MAP_SIZE = 128;

    /**
     * 0号基色的四种颜色均为透明色 并不存储在映射表中
     * mc的颜色映射表(1-34)
     */
    private static final Map<Integer, int[]> COLOR_MAP_LEGACY = new HashMap<>();

    /**
     * 1.8.1新增了一种基色(35)
     */
    private static final Map<Integer, int[]> COLOR_MAP_18_UP = new HashMap<>();

    /**
     * 1.12新增了(36-51)
     */
    private static final Map<Integer, int[]> COLOR_MAP_112_UP = new HashMap<>();

    /**
     * 1.16新增了(52-58)
     */
    private static final Map<Integer, int[]> COLOR_MAP_116_UP = new HashMap<>();

    /**
     * 1.17新增了两种基色(59-60)
     */
    private static final Map<Integer, int[]> COLOR_MAP_117_UP = new HashMap<>();

    static {
        // 基色 RGB Map
        putMapColor(1, 127, 178, 56, COLOR_MAP_LEGACY);
        putMapColor(2, 247, 233, 163, COLOR_MAP_LEGACY);
        putMapColor(3, 199, 199, 199, COLOR_MAP_LEGACY);
        putMapColor(4, 255, 0, 0, COLOR_MAP_LEGACY);
        putMapColor(5, 160, 160, 255, COLOR_MAP_LEGACY);
        putMapColor(6, 167, 167, 167, COLOR_MAP_LEGACY);
        putMapColor(7, 0, 124, 0, COLOR_MAP_LEGACY);
        putMapColor(8, 255, 255, 255, COLOR_MAP_LEGACY);
        putMapColor(9, 164, 168, 184, COLOR_MAP_LEGACY);
        putMapColor(10, 151, 109, 77, COLOR_MAP_LEGACY);
        putMapColor(11, 112, 112, 112, COLOR_MAP_LEGACY);
        putMapColor(12, 64, 64, 255, COLOR_MAP_LEGACY);
        putMapColor(13, 143, 119, 72, COLOR_MAP_LEGACY);
        putMapColor(14, 255, 252, 245, COLOR_MAP_LEGACY);
        putMapColor(15, 216, 127, 51, COLOR_MAP_LEGACY);
        putMapColor(16, 178, 76, 216, COLOR_MAP_LEGACY);
        putMapColor(17, 102, 153, 216, COLOR_MAP_LEGACY);
        putMapColor(18, 229, 229, 51, COLOR_MAP_LEGACY);
        putMapColor(19, 127, 204, 25, COLOR_MAP_LEGACY);
        putMapColor(20, 242, 127, 165, COLOR_MAP_LEGACY);
        putMapColor(21, 76, 76, 76, COLOR_MAP_LEGACY);
        putMapColor(22, 153, 153, 153, COLOR_MAP_LEGACY);
        putMapColor(23, 76, 127, 153, COLOR_MAP_LEGACY);
        putMapColor(24, 127, 63, 178, COLOR_MAP_LEGACY);
        putMapColor(25, 51, 76, 178, COLOR_MAP_LEGACY);
        putMapColor(26, 102, 76, 51, COLOR_MAP_LEGACY);
        putMapColor(27, 102, 127, 51, COLOR_MAP_LEGACY);
        putMapColor(28, 153, 51, 51, COLOR_MAP_LEGACY);
        putMapColor(29, 25, 25, 25, COLOR_MAP_LEGACY);
        putMapColor(30, 250, 238, 77, COLOR_MAP_LEGACY);
        putMapColor(31, 92, 219, 213, COLOR_MAP_LEGACY);
        putMapColor(32, 74, 128, 255, COLOR_MAP_LEGACY);
        putMapColor(33, 0, 217, 58, COLOR_MAP_LEGACY);
        putMapColor(34, 129, 86, 49, COLOR_MAP_LEGACY);
        // 1.8.1+
        putMapColor(35, 112, 2, 0, COLOR_MAP_18_UP);
        // 1.12+
        putMapColor(36, 209, 177, 161, COLOR_MAP_112_UP);
        putMapColor(37, 159, 82, 36, COLOR_MAP_112_UP);
        putMapColor(38, 149, 87, 108, COLOR_MAP_112_UP);
        putMapColor(39, 112, 108, 138, COLOR_MAP_112_UP);
        putMapColor(40, 186, 133, 36, COLOR_MAP_112_UP);
        putMapColor(41, 103, 117, 53, COLOR_MAP_112_UP);
        putMapColor(42, 160, 77, 78, COLOR_MAP_112_UP);
        putMapColor(43, 57, 41, 35, COLOR_MAP_112_UP);
        putMapColor(44, 135, 107, 98, COLOR_MAP_112_UP);
        putMapColor(45, 87, 92, 92, COLOR_MAP_112_UP);
        putMapColor(46, 122, 73, 88, COLOR_MAP_112_UP);
        putMapColor(47, 76, 62, 92, COLOR_MAP_112_UP);
        putMapColor(48, 76, 50, 35, COLOR_MAP_112_UP);
        putMapColor(49, 76, 82, 42, COLOR_MAP_112_UP);
        putMapColor(50, 142, 60, 46, COLOR_MAP_112_UP);
        putMapColor(51, 37, 22, 16, COLOR_MAP_112_UP);
        // 1.16+
        putMapColor(52, 189, 48, 49, COLOR_MAP_116_UP);
        putMapColor(53, 148, 63, 97, COLOR_MAP_116_UP);
        putMapColor(54, 92, 25, 29, COLOR_MAP_116_UP);
        putMapColor(55, 22, 126, 134, COLOR_MAP_116_UP);
        putMapColor(56, 58, 142, 140, COLOR_MAP_116_UP);
        putMapColor(57, 86, 44, 62, COLOR_MAP_116_UP);
        putMapColor(58, 20, 180, 133, COLOR_MAP_116_UP);
        // 1.17+
        putMapColor(59, 86, 86, 86, COLOR_MAP_117_UP);
        putMapColor(60, 186, 150, 126, COLOR_MAP_117_UP);
        putMapColor(61, 127, 167, 150, COLOR_MAP_117_UP);
    }

    private static void putMapColor(int id, int r, int g, int b, Map<Integer, int[]> colorMap) {
        colorMap.put(id * 4, color(0.71, r, g, b));
        colorMap.put(id * 4 + 1, color(0.86, r, g, b));
        colorMap.put(id * 4 + 2, color(1, r, g, b));
        // 1.8.1以前 每个基色的第二种地图色和第四种地图色是一样的
        // 这里不处理这种情况 为1.8以下的版本做单独适配 挺麻烦的
        colorMap.put(id * 4 + 3, color(0.53, r, g, b));
    }

    private static int[] color(double a, int red, int green, int blue) {
        return new int[]{
                (int) (a * red),
                (int) (a * green),
                (int) (a * blue)
        };
    }

    /**
     * 拾色器
     *
     * @param r               红色
     * @param g               绿色
     * @param b               蓝色
     * @param protocolVersion 协议版本
     * @return 与mc中地图色彩最相似的值
     */
    public static byte colorPicker(int r, int g, int b, @NotNull ProtocolVersion protocolVersion) {
        int[] id = {0, 196608};
        colorPicker0(id, r, g, b, COLOR_MAP_LEGACY);
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_8) >= 0) {
            colorPicker0(id, r, g, b, COLOR_MAP_18_UP);
        }
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_12) >= 0) {
            colorPicker0(id, r, g, b, COLOR_MAP_112_UP);
        }
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_16) >= 0) {
            colorPicker0(id, r, g, b, COLOR_MAP_116_UP);
        }
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_17) >= 0) {
            colorPicker0(id, r, g, b, COLOR_MAP_117_UP);
        }
        return (byte) id[0];
    }

    private static void colorPicker0(int[] id, int r, int g, int b, @NotNull Map<Integer, int[]> colorMap) {
        for (Entry<Integer, int[]> en : colorMap.entrySet()) {
            int red = r - en.getValue()[0];
            int green = g - en.getValue()[1];
            int blue = b - en.getValue()[2];

            // R² + G² + B²
            int s = red * red + green * green + blue * blue;
            if (s < id[1]) {
                id[0] = en.getKey().byteValue();
                id[1] = s;
            }
        }
    }

    /**
     * 获取图像数据
     *
     * @param image           图像
     * @param protocolVersion 协议版本
     * @return mc地图数据
     */
    public static byte[] getByte(@NotNull BufferedImage image, @NotNull ProtocolVersion protocolVersion) {
        if (image.getWidth() != MAP_SIZE && image.getHeight() != MAP_SIZE) {
            throw new IllegalArgumentException("image size is not 128!");
        }
        byte[] bytes = new byte[MAP_SIZE * MAP_SIZE];
        int n = 0;
        for (int i = 0; i < MAP_SIZE; i++) {
            for (int j = 0; j < MAP_SIZE; j++) {
                Color color = new Color(image.getRGB(j, i));
                bytes[n] = MapColor.colorPicker(color.getRed(), color.getGreen(), color.getBlue(), protocolVersion);
                n++;
            }
        }
        return bytes;
    }

    /**
     * 获取1.8以前的图像数据
     * 1.8之前 地图只能一列一列更新
     * 所以返回的是一个二维数组
     *
     * @param image 图像
     * @return 数据包
     */
    public static byte[][] getLegacyByte(@NotNull BufferedImage image) {
        if (image.getWidth() != MAP_SIZE && image.getHeight() != MAP_SIZE) {
            throw new IllegalArgumentException("image size is not 128!");
        }

        byte[][] bytes = new byte[MAP_SIZE][];

        for (int i = 0; i < bytes.length; i++) {
            // 前三位数据有其它用途
            byte[] bys = new byte[MAP_SIZE + 3];
            bys[1] = (byte) i;
            for (int j = 0; j < MAP_SIZE; j++) {
                Color color = new Color(image.getRGB(i, j));
                int[] id = {0, 196608};
                MapColor.colorPicker0(id, color.getRed(), color.getGreen(), color.getBlue(), COLOR_MAP_LEGACY);
                bys[j + 3] = (byte) id[0];
            }
            bytes[i] = bys;
        }
        return bytes;
    }
}