package me.xpyex.simplepay.packet.server;

import com.velocitypowered.api.network.ProtocolVersion;
import io.netty.buffer.ByteBuf;
import me.xpyex.simplepay.packet.IPacket;
import me.xpyex.simplepay.packet.entity.Slot;
import me.xpyex.simplepay.packet.entity.WindowAction;
import me.xpyex.simplepay.util.ProtocolUtil;
import me.xpyex.simplepay.util.ProtocolVersions;
import org.jetbrains.annotations.NotNull;

/**
 * 窗口点击数据包
 */
public class WindowClickPacket implements IPacket {

    /**
     * 窗口ID
     */
    private final int windowId;

    /**
     * 插槽位置
     */
    private final int slotId;

    /**
     * 库存操作类型
     */
    private final WindowAction action;

    /**
     * 插槽
     */
    private final Slot slot;

    public WindowClickPacket(int windowId, int slotId, @NotNull WindowAction action, @NotNull Slot slot) {
        this.windowId = windowId;
        this.slotId = slotId;
        this.action = action;
        this.slot = slot;
    }

    public WindowClickPacket(int windowId, int slotId, @NotNull WindowAction action) {
        this(windowId, slotId, action, Slot.EMPTY);
    }

    public WindowClickPacket(int slotId, @NotNull WindowAction action) {
        this(0, slotId, action);
    }

    @Override
    public void encode(@NotNull ByteBuf buf, @NotNull ProtocolVersion protocolVersion) {
        buf.writeByte(windowId);
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_17) > 0) {
            ProtocolUtil.writeVarInt(buf, 0);
        }
        buf.writeShort(slotId);
        buf.writeByte(action.getButton());
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_17) < 0) {
            buf.writeShort(0);
        }
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_9) >= 0) {
            ProtocolUtil.writeVarInt(buf, action.getMode());
        } else {
            buf.writeByte(action.getMode());
        }
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_17) >= 0) {
            ProtocolUtil.writeVarInt(buf, 0);
        }
        slot.write(buf, protocolVersion);
    }
}
