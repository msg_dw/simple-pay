package me.xpyex.simplepay.packet.server;

import com.velocitypowered.api.network.ProtocolVersion;
import io.netty.buffer.ByteBuf;
import me.xpyex.simplepay.packet.IPacket;
import org.jetbrains.annotations.NotNull;

/**
 * 玩家挖掘数据包
 * 丢弃物品也在这个数据包里
 * 这个数据包不提供发送 只接收 拦截
 */
public class PlayerDiggingPacket implements IPacket {
    @Override
    public void encode(@NotNull ByteBuf buf, @NotNull ProtocolVersion protocolVersion) {
        throw new UnsupportedOperationException("此数据包不可发送");
    }
}
