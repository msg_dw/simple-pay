package me.xpyex.simplepay.packet;

import com.velocitypowered.api.network.ProtocolVersion;
import io.netty.buffer.ByteBuf;
import org.jetbrains.annotations.NotNull;

public interface IPacket {

    /**
     * 编码
     * 我不需要解析这些数据包 所以没做解码 这东西调试起来挺麻烦的
     *
     * @param buf             Buf
     * @param protocolVersion 协议版本
     */
    void encode(@NotNull ByteBuf buf, @NotNull ProtocolVersion protocolVersion);
}
