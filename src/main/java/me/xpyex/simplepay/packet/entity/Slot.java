package me.xpyex.simplepay.packet.entity;

import com.velocitypowered.api.network.ProtocolVersion;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.EncoderException;
import lombok.Data;
import me.xpyex.simplepay.util.ComponentUtil;
import me.xpyex.simplepay.util.ProtocolUtil;
import me.xpyex.simplepay.util.ProtocolVersions;
import net.kyori.adventure.nbt.*;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.ComponentSerializer;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

/**
 * 容器的插槽
 */
@Data
public class Slot {

    /**
     * 空物品
     */
    public static final Slot EMPTY = new Slot(0, 0);

    /**
     * 物品ID
     */
    private int itemId;

    /**
     * 物品的子ID
     * 1.13以下物品有此属性
     */
    private int itemSubId;

    /**
     * 物品数量
     */
    private int itemCount;

    /**
     * 物品的名称
     */
    @Nullable
    private Component itemDisplayName;

    /**
     * 物品的Lore
     */
    @Nullable
    private List<Component> itemLore;

    /**
     * 地图ID
     */
    private Integer mapId;

    /**
     * 隐藏附加信息
     */
    private boolean hideAllFlags;

    /**
     * 物品NBT数据
     */
    @NotNull
    private Map<String, BinaryTag> itemData = new HashMap<>();

    public Slot(int itemId) {
        this(itemId, 1);
    }

    public Slot(int itemId, int itemCount) {
        this(itemId, 0, itemCount);
    }

    public Slot(int itemId, int itemSubId, int itemCount) {
        this(itemId, itemSubId, itemCount, null, null);
    }

    public Slot(int itemId, int itemSubId, int itemCount, @Nullable Component itemDisplayName, @Nullable List<Component> itemLore) {
        this.itemId = itemId;
        this.itemSubId = itemSubId;
        this.itemCount = itemCount;
        this.itemDisplayName = itemDisplayName;
        this.itemLore = itemLore;
    }

    /**
     * 添加物品数据
     *
     * @param key key
     * @param tag 标签
     */
    @Deprecated
    public void addItemData(String key, BinaryTag tag) {
        itemData.put(key, tag);
    }

    /**
     * 构建物品数据
     */
    @Nullable
    private CompoundBinaryTag buildItemData(@NotNull ProtocolVersion protocolVersion) {
        Map<String, BinaryTag> finalData = new HashMap<>(itemData);
        Map<String, BinaryTag> display = new HashMap<>(2);
        if (itemDisplayName != null) {
            ComponentSerializer<Component, ? extends Component, String> serializer;
            if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_13) >= 0) {
                serializer = ProtocolUtil.getJsonChatSerializer(protocolVersion);
            } else {
                serializer = LegacyComponentSerializer.legacySection();
            }
            display.put("Name", StringBinaryTag.stringBinaryTag(serializer.serialize(itemDisplayName)));
        }
        if (itemLore != null) {
            ComponentSerializer<Component, ? extends Component, String> serializer;
            if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_14) >= 0) {
                serializer = ProtocolUtil.getJsonChatSerializer(protocolVersion);
            } else {
                serializer = LegacyComponentSerializer.legacySection();
            }
            display.put("Lore", ListBinaryTag.from(itemLore.stream().map(serializer::serialize).map(StringBinaryTag::stringBinaryTag).collect(Collectors.toList())));
        }
        if (!display.isEmpty()) {
            finalData.put("display", CompoundBinaryTag.from(display));
        }
        if (hideAllFlags) {
            finalData.put("HideFlags", IntBinaryTag.intBinaryTag(32));
        }
        if (mapId != null) {
            finalData.put("map", IntBinaryTag.intBinaryTag(0));
        }
        return finalData.isEmpty() ? null : CompoundBinaryTag.from(finalData);
    }

    private void writeComponent(ByteBuf buf, ProtocolVersion protocolVersion) {
        int componentCount = 0, emptyValueComponentCount = 0;

        ByteBuf buffer = Unpooled.buffer();

        if (itemDisplayName != null) {
            ProtocolUtil.writeVarInt(buffer, ComponentUtil.getCustomNameComponentRawId(protocolVersion));
            BinaryTag binaryTag = ComponentUtil.componentToBinaryTag(itemDisplayName);
            ProtocolUtil.writeBinaryTag(buffer, binaryTag, protocolVersion);
            componentCount++;
        }
        if (itemLore != null && !itemLore.isEmpty()) {
            ProtocolUtil.writeVarInt(buffer, ComponentUtil.getLoreComponentRawId(protocolVersion));
            ProtocolUtil.writeVarInt(buffer, itemLore.size());
            for (Component component : itemLore) {
                BinaryTag binaryTag = ComponentUtil.componentToBinaryTag(component);
                ProtocolUtil.writeBinaryTag(buffer, binaryTag, protocolVersion);
            }
            componentCount++;
        }
        if (mapId != null) {
            ProtocolUtil.writeVarInt(buffer, ComponentUtil.getMapIdComponentRawId(protocolVersion));
            ProtocolUtil.writeVarInt(buffer, mapId);
            componentCount++;
        }

        ProtocolUtil.writeVarInt(buf, componentCount);
        ProtocolUtil.writeVarInt(buf, emptyValueComponentCount);
        buf.writeBytes(buffer);
    }

    /**
     * 写入到buf中
     *
     * @param buf 缓存区
     */
    public void write(@NotNull ByteBuf buf, @NotNull ProtocolVersion protocolVersion) {
        if (itemId == 0) {
            if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_13) >= 0) {
                buf.writeBoolean(false);
            } else {
                buf.writeShort(-1);
            }
        }
        else {
            if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_20_5) >= 0) {
                // 数量 id 组件
                ProtocolUtil.writeVarInt(buf, itemCount);
                ProtocolUtil.writeVarInt(buf, itemId);
                writeComponent(buf, protocolVersion);
                return;
            }
            else if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_13) >= 0) {
                // 有物品 id 数量
                buf.writeBoolean(true);
                ProtocolUtil.writeVarInt(buf, itemId);
                buf.writeByte(itemCount);
            }
            else {
                // id 数量 子id
                buf.writeShort(itemId);
                buf.writeByte(itemCount);
                buf.writeShort(itemSubId);
            }
            CompoundBinaryTag tag = buildItemData(protocolVersion);
            if (tag == null) {
                if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_8) >= 0) {
                    buf.writeByte(0);
                } else {
                    buf.writeShort(-1);
                }
            } else {
                if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_8) >= 0) {
                    ProtocolUtil.writeBinaryTag(buf, tag, protocolVersion);
                } else {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    try (DataOutputStream dataOutputStream = new DataOutputStream(new GZIPOutputStream(out))) {
                        BinaryTagIO.writer().write(tag, (DataOutput) dataOutputStream);
                    } catch (IOException e) {
                        throw new EncoderException("Unable to encode NBT CompoundTag");
                    }
                    byte[] bytes = out.toByteArray();
                    buf.writeShort(bytes.length);
                    buf.writeBytes(bytes);
                }
            }
        }
    }
}