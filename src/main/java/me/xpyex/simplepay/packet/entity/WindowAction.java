package me.xpyex.simplepay.packet.entity;

/**
 * 窗口按钮类型
 * 模式2和模式5挺多的 懒得写 反正用不到
 */
public enum WindowAction {
    // 鼠标左键
    MOUSE_LEFT_CLICK(0, 0),
    // 鼠标右键
    MOUSE_RIGHT_CLICK(0, 1),
    // Shift+鼠标左键
    SHIFT_MOUSE_LEFT_CLICK(1, 0),
    // Shift+鼠标右键
    SHIFT_MOUSE_RIGHT_CLICK(1, 1),
    // 鼠标中键点击
    MOUSE_MIDDLE_CLICK(3, 2),
    // Q键 丢弃
    DROP_KEY(4, 0),
    // Ctrl+Q键 全部丢弃
    CTRL_DROP_KEY(4, 1),
    // 鼠标双击
    MOUSE_DOUBLE_CLICK(6, 0);

    /**
     * 模式
     */
    private final int mode;

    /**
     * 按钮
     */
    private final int button;

    WindowAction(int mode, int button) {
        this.mode = mode;
        this.button = button;
    }

    public int getMode() {
        return mode;
    }

    public int getButton() {
        return button;
    }
}
