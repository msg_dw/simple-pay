package me.xpyex.simplepay.packet.client;

import com.velocitypowered.api.network.ProtocolVersion;
import io.netty.buffer.ByteBuf;
import me.xpyex.simplepay.packet.IPacket;
import me.xpyex.simplepay.packet.entity.Slot;
import me.xpyex.simplepay.util.ProtocolUtil;
import me.xpyex.simplepay.util.ProtocolVersions;
import org.jetbrains.annotations.NotNull;

/**
 * 设置库存插槽数据包
 */
public class SetSlotPacket implements IPacket {

    /**
     * 容器ID
     * 0表示背包容器
     */
    private final int windowId;

    /**
     * 插槽位置
     */
    private final int slotIndex;

    /**
     * 插槽
     */
    @NotNull
    private final Slot slot;

    public SetSlotPacket(int slotIndex, @NotNull Slot slot) {
        this(0, slotIndex, slot);
    }

    public SetSlotPacket(int windowId, int slotIndex, @NotNull Slot slot) {
        this.windowId = windowId;
        this.slotIndex = slotIndex;
        this.slot = slot;
    }

    @Override
    public void encode(@NotNull ByteBuf buf, @NotNull ProtocolVersion protocolVersion) {
        ProtocolUtil.writeVarInt(buf, windowId);
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_17_1) >= 0) {
            ProtocolUtil.writeVarInt(buf, 0);
        }
        buf.writeShort(slotIndex);
        slot.write(buf, protocolVersion);
    }
}
