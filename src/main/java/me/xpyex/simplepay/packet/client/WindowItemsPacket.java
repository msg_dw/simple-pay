package me.xpyex.simplepay.packet.client;

import com.velocitypowered.api.network.ProtocolVersion;
import io.netty.buffer.ByteBuf;
import me.xpyex.simplepay.packet.IPacket;
import me.xpyex.simplepay.packet.entity.Slot;
import me.xpyex.simplepay.util.ProtocolUtil;
import me.xpyex.simplepay.util.ProtocolVersions;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * 窗口物品数据包
 * 使用这个前不如看看 能不能另外两位两个解决需求
 *
 * @see SetSlotPacket 设置库存插槽
 * @see HeldChangePacket 更新主手位置
 */
public class WindowItemsPacket implements IPacket {

    /**
     * 容器ID
     * 0表示背包容器
     */
    private final int windowId;

    /**
     * 容器物品
     */
    @NotNull
    private final List<Slot> items;

    public WindowItemsPacket(@NotNull List<Slot> items) {
        this(0, items);
    }

    public WindowItemsPacket(int windowId, @NotNull List<Slot> items) {
        this.windowId = windowId;
        this.items = items;
    }

    @Override
    public void encode(@NotNull ByteBuf buf, @NotNull ProtocolVersion protocolVersion) {
        // 窗口ID
        buf.writeByte(windowId);

        // 1.17.1
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_17_1) >= 0) {
            // 状态ID 不太了解有什么用 1.17.1 新增的
            ProtocolUtil.writeVarInt(buf, 0);
            // 物品的数量 1.17.1 开始就是varint类型了
            ProtocolUtil.writeVarInt(buf, items.size());
        } else {
            // 物品的数量
            buf.writeShort(items.size());
        }

        // 每一个物品
        for (Slot slot : items) {
            slot.write(buf, protocolVersion);
        }

        // 1.17.1 就挺离谱的 1.17 都没有
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_17_1) >= 0) {
            Slot.EMPTY.write(buf, protocolVersion);
        }
    }
}
