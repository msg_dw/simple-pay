package me.xpyex.simplepay.packet.client;

import com.velocitypowered.api.network.ProtocolVersion;
import io.netty.buffer.ByteBuf;
import me.xpyex.simplepay.packet.IPacket;
import me.xpyex.simplepay.util.ProtocolUtil;
import org.jetbrains.annotations.NotNull;

/**
 * 1.7 狗都不玩
 */
public class LegacyMapViewPacket implements IPacket {

    /**
     * 地图ID
     */
    private final int mapId;

    /**
     * 地图的数据
     */
    private final byte[] data;

    public LegacyMapViewPacket(byte[] data) {
        this(0, data);
    }

    public LegacyMapViewPacket(int mapId, byte[] data) {
        this.mapId = mapId;
        this.data = data;
    }

    @Override
    public void encode(@NotNull ByteBuf buf, @NotNull ProtocolVersion protocolVersion) {
        ProtocolUtil.writeVarInt(buf, mapId);
        buf.writeShort(data.length);
        buf.writeBytes(data);
    }

}
