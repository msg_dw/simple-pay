package me.xpyex.simplepay.packet.client;

import com.velocitypowered.api.network.ProtocolVersion;
import io.netty.buffer.ByteBuf;
import me.xpyex.simplepay.packet.IPacket;
import me.xpyex.simplepay.util.ProtocolUtil;
import me.xpyex.simplepay.util.ProtocolVersions;
import org.jetbrains.annotations.NotNull;

/**
 * 玩家位置包
 */
public class PlayerPositionPacket implements IPacket {

    private final double x;
    private final double y;
    private final double z;
    private final float yaw;
    private final float pitch;
    private final boolean absolute;

    public PlayerPositionPacket(double x, double y, double z, float yaw, float pitch) {
        this(x, y, z, yaw, pitch, false);
    }

    public PlayerPositionPacket(double x, double y, double z, float yaw, float pitch, boolean absolute) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
        this.absolute = absolute;
    }

    @Override
    public void encode(@NotNull ByteBuf buf, @NotNull ProtocolVersion protocolVersion) {
        // 1.21以上 传送id改到了首位
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_21_4) >= 0) {
            ProtocolUtil.writeVarInt(buf, 0);
        }

        // 坐标都是一样的
        buf.writeDouble(x);
        buf.writeDouble(y);
        buf.writeDouble(z);

        // 速度 相对都是0
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_21_4) >= 0) {
            buf.writeDouble(0);
            buf.writeDouble(0);
            buf.writeDouble(0);
        }

        buf.writeFloat(yaw);
        buf.writeFloat(pitch);

        // 1.8 以上支持 相对位置
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_8) >= 0) {
            // Flags
            int flags = 0;
            if (!absolute) {
                flags = 0x01 | 0x02 | 0x04 | 0x08;
                if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_21_4) >= 0) {
                    // 高版本还得加上 速度的相对位置
                    flags |= 0x20 | 0x40 | 0x80;
                }
            }
            if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_21_4) >= 0) {
                buf.writeInt(flags);
            } else {
                buf.writeByte(flags);
            }
        } else {
            // onGround
            buf.writeBoolean(false);
        }

        // 1.9 以上需要一个 传送的ID
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_9) >= 0 && ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_21_4) < 0) {
            ProtocolUtil.writeVarInt(buf, 0);
        }

        // 1.17 以上 需要一个是否下马属性 (奇怪的是1.19.4好像移除了这个属性)
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_17) >= 0 && ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_19_4) < 0) {
            buf.writeBoolean(false);
        }
    }
}
