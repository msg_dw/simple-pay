package me.xpyex.simplepay.packet.client;

import com.velocitypowered.api.network.ProtocolVersion;
import io.netty.buffer.ByteBuf;
import me.xpyex.simplepay.packet.IPacket;
import me.xpyex.simplepay.util.ProtocolUtil;
import me.xpyex.simplepay.util.ProtocolVersions;
import org.jetbrains.annotations.NotNull;

/**
 * 地图视图数据包
 *
 * @see LegacyMapViewPacket 1.8以下的版本
 */
public class MapViewPacket implements IPacket {

    /**
     * 地图ID
     */
    private final int mapId;

    /**
     * 地图的图像
     */
    private final byte[] imageData;

    public MapViewPacket(byte[] imageData) {
        this(0, imageData);
    }

    public MapViewPacket(int mapId, byte[] imageData) {
        this.mapId = mapId;
        this.imageData = imageData;
    }

    @Override
    public void encode(@NotNull ByteBuf buf, @NotNull ProtocolVersion protocolVersion) {
        // 地图ID
        ProtocolUtil.writeVarInt(buf, mapId);

        // 地图的尺寸
        buf.writeByte(0);

        // 版本 >= 1.14 时
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_14) >= 0) {
            // 制图台有个锁定功能(不能开启)
            buf.writeBoolean(false);
        }

        // 版本 >= 1.9 时
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_9) >= 0) {
            // 是否要显示图标
            buf.writeBoolean(false);
        }
        // 小于 1.17 时 无论显示图标是否开启 都需要写入图标
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_17) < 0) {
            // 图标的数量和图标(不要)
            ProtocolUtil.writeVarInt(buf, 0);
        }
        // 大小和坐标
        buf.writeByte(128);
        buf.writeByte(128);
        buf.writeByte(0);
        buf.writeByte(0);

        // 地图图像
        ProtocolUtil.writeByteArray(buf, imageData);
    }
}
