package me.xpyex.simplepay.packet.client;

import com.velocitypowered.api.network.ProtocolVersion;
import io.netty.buffer.ByteBuf;
import me.xpyex.simplepay.packet.IPacket;
import org.jetbrains.annotations.NotNull;

/**
 * 更新主手位置数据包
 */
public class HeldChangePacket implements IPacket {

    /**
     * 玩家库存位置
     * 0-8
     */
    private final int heldIndex;

    public HeldChangePacket() {
        this(0);
    }

    public HeldChangePacket(int heldIndex) {
        this.heldIndex = heldIndex;
    }

    @Override
    public void encode(@NotNull ByteBuf buf, @NotNull ProtocolVersion protocolVersion) {
        buf.writeByte(heldIndex);
    }
}
