package me.xpyex.simplepay.packet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.velocitypowered.api.network.ProtocolVersion;
import com.velocitypowered.api.proxy.Player;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import me.xpyex.simplepay.config.Localization;
import me.xpyex.simplepay.domain.Order;
import me.xpyex.simplepay.packet.client.*;
import me.xpyex.simplepay.packet.entity.Slot;
import me.xpyex.simplepay.packet.entity.WindowAction;
import me.xpyex.simplepay.packet.server.PlayerDiggingPacket;
import me.xpyex.simplepay.packet.server.WindowClickPacket;
import me.xpyex.simplepay.pay.PayMethod;
import me.xpyex.simplepay.util.MapColor;
import me.xpyex.simplepay.util.ProtocolUtil;
import me.xpyex.simplepay.util.ProtocolVersions;
import net.kyori.adventure.nbt.IntBinaryTag;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.awt.image.BufferedImage;
import java.util.*;

/**
 * 数据包管理器
 */
@Singleton
public class PacketManager {

    private final Map<Class<? extends IPacket>, Map<ProtocolVersion, Integer>> packetRegistry = new HashMap<>();

    @Inject
    private Logger logger;

    @Inject
    private Localization l10n;

    public PacketManager() {
        registerPacket(HeldChangePacket.class, map(ProtocolVersions.MINECRAFT_1_7_2, 0x09),
                map(ProtocolVersions.MINECRAFT_1_9, 0x37), map(ProtocolVersions.MINECRAFT_1_12, 0x39),
                map(ProtocolVersions.MINECRAFT_1_12_1, 0x3A), map(ProtocolVersions.MINECRAFT_1_13, 0x3D),
                map(ProtocolVersions.MINECRAFT_1_14, 0x3F), map(ProtocolVersions.MINECRAFT_1_15, 0x40),
                map(ProtocolVersions.MINECRAFT_1_16, 0x3F), map(ProtocolVersions.MINECRAFT_1_17, 0x48),
                map(ProtocolVersions.MINECRAFT_1_19, 0x47), map(ProtocolVersions.MINECRAFT_1_19_1, 0x4A),
                map(ProtocolVersions.MINECRAFT_1_19_3, 0x49), map(ProtocolVersions.MINECRAFT_1_19_4, 0x4D),
                map(ProtocolVersions.MINECRAFT_1_20_2, 0x4F), map(ProtocolVersions.MINECRAFT_1_20_3, 0x51),
                map(ProtocolVersions.MINECRAFT_1_20_5, 0x53), map(ProtocolVersions.MINECRAFT_1_21_4, 0x63));
        registerPacket(LegacyMapViewPacket.class, map(ProtocolVersions.MINECRAFT_1_7_2, 0x34));
        registerPacket(MapViewPacket.class, map(ProtocolVersions.MINECRAFT_1_7_2, 0x34),
                map(ProtocolVersions.MINECRAFT_1_9, 0x24), map(ProtocolVersions.MINECRAFT_1_13, 0x26),
                map(ProtocolVersions.MINECRAFT_1_15, 0x27), map(ProtocolVersions.MINECRAFT_1_16, 0x26),
                map(ProtocolVersions.MINECRAFT_1_16_2, 0x25), map(ProtocolVersions.MINECRAFT_1_17, 0x27),
                map(ProtocolVersions.MINECRAFT_1_19, 0x24), map(ProtocolVersions.MINECRAFT_1_19_1, 0x26),
                map(ProtocolVersions.MINECRAFT_1_19_3, 0x25), map(ProtocolVersions.MINECRAFT_1_19_4, 0x29),
                map(ProtocolVersions.MINECRAFT_1_20_2, 0x2A), map(ProtocolVersions.MINECRAFT_1_20_5, 0x2C),
                map(ProtocolVersions.MINECRAFT_1_21_4, 0x2D));
        registerPacket(SetSlotPacket.class, map(ProtocolVersions.MINECRAFT_1_7_2, 0x2F),
                map(ProtocolVersions.MINECRAFT_1_9, 0x16), map(ProtocolVersions.MINECRAFT_1_13, 0x17),
                map(ProtocolVersions.MINECRAFT_1_14, 0x16), map(ProtocolVersions.MINECRAFT_1_15, 0x17),
                map(ProtocolVersions.MINECRAFT_1_16, 0x16), map(ProtocolVersions.MINECRAFT_1_16_2, 0x15),
                map(ProtocolVersions.MINECRAFT_1_17, 0x16), map(ProtocolVersions.MINECRAFT_1_19, 0x13),
                map(ProtocolVersions.MINECRAFT_1_19_3, 0x12), map(ProtocolVersions.MINECRAFT_1_19_4, 0x14),
                map(ProtocolVersions.MINECRAFT_1_20_2, 0x15));
        registerPacket(WindowItemsPacket.class, map(ProtocolVersions.MINECRAFT_1_7_2, 0x30),
                map(ProtocolVersions.MINECRAFT_1_9, 0x14), map(ProtocolVersions.MINECRAFT_1_13, 0x15),
                map(ProtocolVersions.MINECRAFT_1_14, 0x14), map(ProtocolVersions.MINECRAFT_1_15, 0x15),
                map(ProtocolVersions.MINECRAFT_1_16, 0x14), map(ProtocolVersions.MINECRAFT_1_16_2, 0x13),
                map(ProtocolVersions.MINECRAFT_1_17, 0x14), map(ProtocolVersions.MINECRAFT_1_19, 0x11),
                map(ProtocolVersions.MINECRAFT_1_19_3, 0x10), map(ProtocolVersions.MINECRAFT_1_19_4, 0x12),
                map(ProtocolVersions.MINECRAFT_1_20_2, 0x13));
        registerPacket(WindowClickPacket.class, map(ProtocolVersions.MINECRAFT_1_7_2, 0x0E),
                map(ProtocolVersions.MINECRAFT_1_9, 0x07), map(ProtocolVersions.MINECRAFT_1_12, 0x08),
                map(ProtocolVersions.MINECRAFT_1_12_1, 0x07), map(ProtocolVersions.MINECRAFT_1_13, 0x08),
                map(ProtocolVersions.MINECRAFT_1_14, 0x09), map(ProtocolVersions.MINECRAFT_1_17, 0x08),
                map(ProtocolVersions.MINECRAFT_1_19, 0x0A), map(ProtocolVersions.MINECRAFT_1_19_1, 0x0B),
                map(ProtocolVersions.MINECRAFT_1_19_3, 0x0A), map(ProtocolVersions.MINECRAFT_1_19_4, 0x0B),
                map(ProtocolVersions.MINECRAFT_1_20_2, 0x0D), map(ProtocolVersions.MINECRAFT_1_20_5, 0x0E),
                map(ProtocolVersions.MINECRAFT_1_21_4, 0x10));
        registerPacket(PlayerDiggingPacket.class, map(ProtocolVersions.MINECRAFT_1_7_2, 0x07),
                map(ProtocolVersions.MINECRAFT_1_9, 0x13), map(ProtocolVersions.MINECRAFT_1_12, 0x14),
                map(ProtocolVersions.MINECRAFT_1_13, 0x18), map(ProtocolVersions.MINECRAFT_1_14, 0x1A),
                map(ProtocolVersions.MINECRAFT_1_16, 0x1B), map(ProtocolVersions.MINECRAFT_1_17, 0x1A),
                map(ProtocolVersions.MINECRAFT_1_19, 0x1C), map(ProtocolVersions.MINECRAFT_1_19_1, 0x1D),
                map(ProtocolVersions.MINECRAFT_1_19_3, 0x1C), map(ProtocolVersions.MINECRAFT_1_19_4, 0x1D),
                map(ProtocolVersions.MINECRAFT_1_20_2, 0x20), map(ProtocolVersions.MINECRAFT_1_20_3, 0x21),
                map(ProtocolVersions.MINECRAFT_1_20_5, 0x24), map(ProtocolVersions.MINECRAFT_1_21_4, 0x27));
        registerPacket(PlayerPositionPacket.class, map(ProtocolVersions.MINECRAFT_1_7_2, 0x08),
                map(ProtocolVersions.MINECRAFT_1_9, 0x2E), map(ProtocolVersions.MINECRAFT_1_12_1, 0x2F),
                map(ProtocolVersions.MINECRAFT_1_13, 0x32), map(ProtocolVersions.MINECRAFT_1_14, 0x35),
                map(ProtocolVersions.MINECRAFT_1_15, 0x36), map(ProtocolVersions.MINECRAFT_1_16, 0x35),
                map(ProtocolVersions.MINECRAFT_1_16_2, 0x34), map(ProtocolVersions.MINECRAFT_1_17, 0x38),
                map(ProtocolVersions.MINECRAFT_1_19, 0x36), map(ProtocolVersions.MINECRAFT_1_19_1, 0x39),
                map(ProtocolVersions.MINECRAFT_1_19_3, 0x38), map(ProtocolVersions.MINECRAFT_1_19_4, 0x3C),
                map(ProtocolVersions.MINECRAFT_1_20_2, 0x3E), map(ProtocolVersions.MINECRAFT_1_20_5, 0x40),
                map(ProtocolVersions.MINECRAFT_1_21_4, 0x42));
    }

    /**
     * 获取数据包ID
     *
     * @param packet          数据包
     * @param protocolVersion 协议版本
     */
    public int getPacketId(@NotNull Class<? extends IPacket> packet, @NotNull ProtocolVersion protocolVersion) {
        Map<ProtocolVersion, Integer> packetMapping = packetRegistry.get(packet);
        if (packetMapping == null) {
            throw new IllegalStateException("无效的数据包类型");
        }
        Integer integer = packetMapping.get(protocolVersion);
        if (integer == null) {
            throw new IllegalStateException("无效的数据包类型");
        }
        return integer;
    }

    /**
     * 发送数据包
     *
     * @param player 玩家
     * @param packet 数据包
     */
    public void sendPacket(@NotNull Player player, @NotNull IPacket packet) {
        sendPacket(ProtocolUtil.getConnection(player), packet);
    }

    /**
     * 发送数据包
     *
     * @param connection 连接
     * @param packet     数据包
     */
    private void sendPacket(@NotNull Object connection, @NotNull IPacket packet) {
        ByteBuf buf = Unpooled.buffer();
        ProtocolVersion protocolVersion = ProtocolUtil.getProtocolVersion(connection);
        ProtocolUtil.writeVarInt(buf, getPacketId(packet.getClass(), protocolVersion));
        packet.encode(buf, protocolVersion);
        ProtocolUtil.connectionWrite(connection, buf);
    }

    /**
     * 发送地图视图包
     *
     * @param player 玩家
     * @param image  图像
     */
    public void sendMapViewPacket(@NotNull Player player, @NotNull BufferedImage image) {
        ProtocolVersion protocolVersion = player.getProtocolVersion();
        if (protocolVersion.compareTo(ProtocolVersion.MINECRAFT_1_8) >= 0) {
            sendPacket(player, new MapViewPacket(MapColor.getByte(image, protocolVersion)));
        } else {
            byte[][] legacyByte = MapColor.getLegacyByte(image);
            for (byte[] bytes : legacyByte) {
                sendPacket(player, new LegacyMapViewPacket(bytes));
            }
        }
    }

    /**
     * 发送地图物品包
     *
     * @param player 玩家
     */
    public void sendMapItemPacket(@NotNull Player player, @NotNull Order order, @NotNull PayMethod payMethod) {
        ProtocolVersion protocolVersion = player.getProtocolVersion();
        int itemId;
        if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_21_4) >= 0) {
            // 1.21.4 从这个版本开始 并不要求物品必须是 地图 其它物品也可以正常显示地图
            itemId = 1031;
        } else if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_20_5) >= 0) {
            // 1.20.5 - 1.21
            itemId = 982;
        } else if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_20_3) >= 0) {
            // 1.20.3 - 1.20.4
            itemId = 979;
        } else if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_20) >= 0) {
            // 1.20 - 1.20.2
            itemId = 941;
        } else if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_19_4) >= 0) {
            // 1.19.4
            itemId = 937;
        } else if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_19) >= 0) {
            // 1.19 - 1.19.3
            itemId = 914;
        } else if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_17) >= 0) {
            // 1.17 - 1.18.2
            itemId = 847;
        } else if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_16) >= 0) {
            // 1.16 - 1.16.5
            itemId = 733;
        } else if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_14) >= 0) {
            // 1.14 - 1.15.2
            itemId = 671;
        } else if (ProtocolUtil.compare(protocolVersion, ProtocolVersions.MINECRAFT_1_13) >= 0) {
            // 1.13 - 1.13.2
            itemId = 613;
        } else {
            // 1.13 以前全是 358
            itemId = 358;
        }
        Component nameComponent = null;
        List<Component> lores = null;
        Map<String, Object> replaceTable = Map.of(
                "%player%", player.getUsername(),
                "%server%", order.getServer(),
                "%money%", order.getTotalFee(),
                "%pay-method%", payMethod.getName()
        );
        String name = l10n.parse("pay.map-item.name", replaceTable);
        if (!name.isEmpty()) {
            nameComponent = LegacyComponentSerializer.legacyAmpersand().deserialize(name);
        }
        String lore = l10n.parse("pay.map-item.lore", replaceTable);
        if (!lore.isEmpty()) {
            lores = new ArrayList<>();
            for (String line : lore.split("\n")) {
                lores.add(LegacyComponentSerializer.legacyAmpersand().deserialize(line));
            }
        }
        Slot slot = new Slot(itemId, 0, 1, nameComponent, lores);
        if (protocolVersion.compareTo(ProtocolVersion.MINECRAFT_1_8) >= 0) {
            slot.setHideAllFlags(true);
        }
        if (protocolVersion.compareTo(ProtocolVersion.MINECRAFT_1_13) >= 0) {
            slot.setMapId(0);
        }
        sendPacket(player, new SetSlotPacket(40, slot));
        sendPacket(player, new HeldChangePacket(4));
    }

    /**
     * 发送销毁物品包
     *
     * @param player 玩家
     */
    public void sendDestroyItemPacket(@NotNull Player player) {
        // 向服务器发送数据包 让其更新玩家背包物品 （因为我们并不知道玩家背包有哪些物品）
        Object connection = ProtocolUtil.getServerConnection(player);
        if (connection != null) {
            if (player.getProtocolVersion().compareTo(ProtocolVersion.MINECRAFT_1_17) >= 0) {
                // 1.17以上 基本上窗口操作都会更新背包物品
                // 但是直接发送一个鼠标中间点击屏幕外边的包 不会对游戏由任何影响
                sendPacket(connection, new WindowClickPacket(-999, WindowAction.MOUSE_MIDDLE_CLICK));
            } else {
                // 1.17以下版本 我没有找到比较好的方式
                // 只能点击原物品两次
                WindowClickPacket windowClickPacket = new WindowClickPacket(
                        0,
                        40,
                        WindowAction.MOUSE_LEFT_CLICK,
                        // 还不能是空物品 否则如果服务器检测到玩家背包本来就是空的 则不会发送更新包
                        new Slot(1)
                );
                sendPacket(connection, windowClickPacket);
                sendPacket(connection, windowClickPacket);
            }
        }
    }

    /**
     * 发送低头数据包
     *
     * @param player 玩家
     */
    public void sendHeadDownPacket(@NotNull Player player) {
        if (player.getProtocolVersion().compareTo(ProtocolVersion.MINECRAFT_1_8) >= 0) {
            sendPacket(player, new PlayerPositionPacket(0.0, 0.0, 0.0, 0.0F, 50.0F));
        } else {
            // 1.8以下的版本 不支持相对位置的调整
            logger.warn("目标玩家 {} 的客户端暂不支持HeadDown功能", player.getUsername());
        }
    }

    private <T extends IPacket> void registerPacket(Class<T> clazz, PacketMapping... mappings) {
        Map<ProtocolVersion, Integer> packetMapping = new EnumMap<>(ProtocolVersion.class);
        for (PacketMapping mapping : mappings) {
            if (mapping == PacketMapping.EMPTY_PACKET_MAPPING) {
                continue;
            }
            for (ProtocolVersion protocolVersion : ProtocolVersion.values()) {
                if (mapping.protocolVersion.compareTo(protocolVersion) <= 0) {
                    packetMapping.put(protocolVersion, mapping.packetId);
                }
            }
        }
        packetRegistry.put(clazz, packetMapping);
    }

    private PacketMapping map(ProtocolVersions protocol, int packetId) {
        if (protocol.isSupported()) {
            return new PacketMapping(protocol.getProtocolVersion(), packetId);
        }
        return PacketMapping.EMPTY_PACKET_MAPPING;
    }

    private static class PacketMapping {
        private final static PacketMapping EMPTY_PACKET_MAPPING = new PacketMapping(null, 0);
        private final ProtocolVersion protocolVersion;
        private final int packetId;

        public PacketMapping(ProtocolVersion protocolVersion, int packetId) {
            this.protocolVersion = protocolVersion;
            this.packetId = packetId;
        }
    }
}
