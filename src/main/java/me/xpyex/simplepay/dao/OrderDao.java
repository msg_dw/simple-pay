package me.xpyex.simplepay.dao;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.xpyex.simplepay.config.SimplePayConfig.StorageConfig;
import me.xpyex.simplepay.domain.Order;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class OrderDao {

    /**
     * 数据库的表名
     */
    public static final String TABLE_NAME = "pay_order";

    @Inject
    private StorageConfig config;

    /**
     * 向数据库里面插入一条订单记录
     *
     * @param order 订单
     * @return 是否成功
     */
    public boolean insert(@NotNull Order order) {
        try (
                Connection connection = DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
                PreparedStatement statement = connection.prepareStatement("INSERT INTO `" + TABLE_NAME + "` (`id`,`server`,`buyer`,`subject`,`total_fee`,`status`,`ship`,`create_time`,`pay_time`,`pay_info`) VALUES (?,?,?,?,?,?,?,?,?,?);")
        ) {
            statement.setLong(1, order.getId());
            statement.setString(2, order.getServer());
            statement.setString(3, order.getBuyer());
            statement.setString(4, order.getSubject());
            statement.setDouble(5, order.getTotalFee());
            statement.setInt(6, order.getStatus());
            statement.setInt(7, order.getShip());
            statement.setTimestamp(8, order.getCreateTime());
            statement.setTimestamp(9, null);
            statement.setString(10, null);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 通过订单号去获取一个订单
     *
     * @param id 订单号
     * @return 订单
     */
    @Nullable
    public Order selectById(long id) {
        try (
                Connection connection = DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
                PreparedStatement statement = connection.prepareStatement("SELECT `id`,`server`,`buyer`,`subject`,`total_fee`,`status`,`ship`,`create_time`,`pay_time`,`pay_info` FROM " + TABLE_NAME + " WHERE `id` = ?;")
        ) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Order order = new Order();
                    order.setId(resultSet.getLong(1));
                    order.setServer(resultSet.getString(2));
                    order.setBuyer(resultSet.getString(3));
                    order.setSubject(resultSet.getString(4));
                    order.setTotalFee(resultSet.getDouble(5));
                    order.setStatus(resultSet.getInt(6));
                    order.setShip(resultSet.getInt(7));
                    order.setCreateTime(resultSet.getTimestamp(8));
                    order.setPayTime(resultSet.getTimestamp(9));
                    order.setPayInfo(resultSet.getString(10));
                    return order;
                }
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 标记付款
     *
     * @param id   订单号
     * @param info 支付信息
     * @return 是否成功
     */
    public boolean markPay(long id, String info) {
        try (
                Connection connection = DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
                PreparedStatement statement = connection.prepareStatement("UPDATE `" + TABLE_NAME + "` SET `status` = 1, `pay_time` = ?, `pay_info` = ? WHERE `id` = ? AND `status` = 0;")
        ) {
            statement.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            statement.setString(2, info);
            statement.setLong(3, id);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 标记发货
     *
     * @param id 订单号
     * @return 是否成功
     */
    public boolean markShip(long id) {
        try (
                Connection connection = DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
                PreparedStatement statement = connection.prepareStatement("UPDATE `" + TABLE_NAME + "` SET `ship` = 1 WHERE `id` = ? AND `status` = 1 AND `ship` = 0;")
        ) {
            statement.setLong(1, id);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 获取未发货的订单
     * @param buyer 买家
     * @return 订单
     */
    public List<Order> selectUnshipOrder(String buyer) {
        try (
                Connection connection = DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
                PreparedStatement statement = connection.prepareStatement("SELECT `id`,`server`,`buyer`,`subject`,`total_fee`,`status`,`ship`,`create_time`,`pay_time`,`pay_info` FROM " + TABLE_NAME + " WHERE `buyer` = ? AND `status` = 1 AND `ship` = 0;")
        ) {
            statement.setString(1, buyer);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Order> orders = new ArrayList<>();
                if (resultSet.next()) {
                    Order order = new Order();
                    order.setId(resultSet.getLong(1));
                    order.setServer(resultSet.getString(2));
                    order.setBuyer(resultSet.getString(3));
                    order.setSubject(resultSet.getString(4));
                    order.setTotalFee(resultSet.getDouble(5));
                    order.setStatus(resultSet.getInt(6));
                    order.setShip(resultSet.getInt(7));
                    order.setCreateTime(resultSet.getTimestamp(8));
                    order.setPayTime(resultSet.getTimestamp(9));
                    order.setPayInfo(resultSet.getString(10));
                    orders.add(order);
                }
                return orders;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
