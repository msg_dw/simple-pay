package me.xpyex.simplepay.dao;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.xpyex.simplepay.config.SimplePayConfig.StorageConfig;
import me.xpyex.simplepay.domain.PlayerData;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Singleton
public class PlayerInfoDao {

    /**
     * 数据库的表名
     */
    public static final String TABLE_NAME = "pay_player_info";

    @Inject
    private StorageConfig config;

    /**
     * 插入或者更新
     *
     * @param uuid UUID
     * @param name 玩家名
     * @return 是否成功
     */
    public boolean insertOrUpdate(@NotNull UUID uuid, String name) {
        return insertOrUpdate(uuid.toString(), name);
    }

    /**
     * 插入或者更新
     *
     * @param uuid UUID
     * @param name 玩家名
     * @return 是否成功
     */
    public boolean insertOrUpdate(@NotNull String uuid, String name) {
        try (
                Connection connection = DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
                PreparedStatement findStatement = connection.prepareStatement("SELECT count(*) FROM " + TABLE_NAME + " WHERE `uuid` = ?;")
        ) {
            findStatement.setString(1, uuid);
            ResultSet resultSet = findStatement.executeQuery();
            if (resultSet.next()) {
                if (resultSet.getInt(1) == 0) {
                    try (PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO `" + TABLE_NAME + "` (`uuid`,`name`) VALUES (?,?);")) {
                        insertStatement.setString(1, uuid);
                        insertStatement.setString(2, name);
                        return insertStatement.executeUpdate() > 0;
                    }
                } else {
                    try (PreparedStatement updateStatement = connection.prepareStatement("UPDATE `" + TABLE_NAME + "` SET `name` = ? WHERE `uuid` = ?;")) {
                        updateStatement.setString(1, name);
                        updateStatement.setString(2, uuid);
                        return updateStatement.executeUpdate() > 0;
                    }
                }
            }
            return false;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 通过UUID获取玩家信息
     *
     * @param uuid UUID
     * @return 玩家名
     */
    @Nullable
    public String selectNameByUuid(@NotNull UUID uuid) {
        return selectNameByUuid(uuid.toString());
    }

    /**
     * 通过UUID获取玩家信息
     *
     * @param uuid UUID字符串
     * @return 玩家信息
     */
    @Nullable
    public String selectNameByUuid(@NotNull String uuid) {
        try (
                Connection connection = DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
                PreparedStatement statement = connection.prepareStatement("SELECT `name` FROM " + TABLE_NAME + " WHERE `uuid` = ?;")
        ) {
            statement.setString(1, uuid);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getString(1);
                }
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 通过名字获取玩家信息
     *
     * @param name 名字
     * @return 玩家信息
     */
    @Nullable
    public UUID selectUuidByName(@NotNull String name) {
        try (
                Connection connection = DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
                PreparedStatement statement = connection.prepareStatement("SELECT `uuid` FROM " + TABLE_NAME + " WHERE `name` = ?;")
        ) {
            statement.setString(1, name);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return UUID.fromString(resultSet.getString(1));
                }
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 获取排名
     *
     * @param size 取多少位
     * @return 排名
     */
    @NotNull
    public List<PlayerData> getRank(int size) {
        try (
                Connection connection = DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
                PreparedStatement statement = connection.prepareStatement(
                        "SELECT buyer,name,amount\n" +
                                "FROM (\n" +
                                "SELECT `buyer`, SUM(`total_fee`) AS `amount`\n" +
                                "FROM `" + OrderDao.TABLE_NAME + "`\n" +
                                "WHERE `status` = 1\n" +
                                "AND `ship` = 1\n" +
                                "GROUP BY `buyer`\n" +
                                ") AS `rank`\n" +
                                "LEFT JOIN `" + TABLE_NAME + "` \n" +
                                "ON `buyer` = `uuid`\n" +
                                "ORDER BY `amount` DESC\n" +
                                "LIMIT ?;"
                )
        ) {
            statement.setInt(1, size);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<PlayerData> datas = new ArrayList<>();
                while (resultSet.next()) {
                    PlayerData playerData = new PlayerData();
                    playerData.setUuid(UUID.fromString(resultSet.getString(1)));
                    playerData.setName(resultSet.getString(2));
                    playerData.setAmount(resultSet.getDouble(3));
                    datas.add(playerData);
                }
                return datas;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
