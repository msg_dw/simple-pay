package me.xpyex.simplepay.pay;

import com.google.inject.Injector;
import me.xpyex.simplepay.domain.Order;
import me.xpyex.simplepay.http.HttpHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * 支付方式
 */
public interface PayMethod {

    /**
     * @return 支付方式的名称
     */
    @NotNull
    String getName();

    /**
     * 获取支付方式的建议
     * 可用于命令补全或其它
     *
     * @return 支付方式的建议
     */
    @NotNull
    List<String> getSuggest();

    /**
     * 获取通知前缀
     * {@link me.xpyex.simplepay.common.Constants}
     *
     * @return 通知前缀
     */
    @NotNull
    String getNotifyPrefix();

    /**
     * 创建支付方式的HTTP处理器
     *
     * @return HTTP处理器
     */
    @NotNull
    HttpHandler createHandler(Injector injector);

    /**
     * 发起Native支付
     * 微信的Native支付 不能从相册选择二维码扫码付款
     *
     * @param order 订单
     * @return 二维码URL
     */
    @Nullable
    String initiateNativePay(@NotNull Order order);
}
