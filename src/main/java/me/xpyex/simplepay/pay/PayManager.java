package me.xpyex.simplepay.pay;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.velocitypowered.api.event.EventManager;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.ServerConnection;
import me.xpyex.simplepay.common.Constants;
import me.xpyex.simplepay.config.Configuration;
import me.xpyex.simplepay.config.Localization;
import me.xpyex.simplepay.config.SimplePayConfig.PayConfig;
import me.xpyex.simplepay.config.SimplePayConfig.ShipConfig;
import me.xpyex.simplepay.dao.OrderDao;
import me.xpyex.simplepay.dao.PlayerInfoDao;
import me.xpyex.simplepay.domain.Order;
import me.xpyex.simplepay.event.OrderPaymentEvent;
import me.xpyex.simplepay.event.OrderShipEvent;
import me.xpyex.simplepay.event.QrCodeCreateEvent;
import me.xpyex.simplepay.http.HttpServer;
import me.xpyex.simplepay.packet.PacketManager;
import me.xpyex.simplepay.pay.method.AliPayMethod;
import me.xpyex.simplepay.pay.method.TenPayMethod;
import me.xpyex.simplepay.pay.method.UnionPayMethod;
import me.xpyex.simplepay.pay.method.WeChatMethod;
import me.xpyex.simplepay.util.FastQrCodeUtil;
import me.xpyex.simplepay.util.ProtocolUtil;
import me.xpyex.simplepay.util.Sequence;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.awt.image.BufferedImage;
import java.sql.Timestamp;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 支付管理
 */
@Singleton
public class PayManager {

    /**
     * 支付方式
     */
    private final ArrayList<PayMethod> payMethods = new ArrayList<>();

    /**
     * 正在支付的集合
     */
    private final ArrayList<UUID> paying = new ArrayList<>();

    /**
     * 雪花算法
     */
    private final Sequence sequence = new Sequence(0, 0);

    @Inject
    private Logger logger;

    @Inject
    private ProxyServer server;

    @Inject
    private EventManager eventManager;

    @Inject
    private PacketManager packetManager;

    @Inject
    private ShipConfig shipConfig;

    @Inject
    private PayConfig payConfig;

    @Inject
    private OrderDao orderDao;

    @Inject
    private PlayerInfoDao playerInfoDao;

    @Inject
    private Localization l10n;

    @Inject
    private Injector injector;

    private boolean hasUnshipOrder = true;

    /**
     * 配置
     *
     * @param config 商户配置
     */
    public void setup(Configuration config, String notifyUrl) {
        payMethods.clear();
        Injector childInjector = injector.createChildInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(String.class).annotatedWith(Names.named("NotifyUrl")).toInstance(notifyUrl);
            }
        });
        HttpServer httpServer = childInjector.getInstance(HttpServer.class);
        Configuration alipayConfig = config.getConfiguration("alipay");
        if (alipayConfig.getBoolean("enable")) {
            PayMethod payMethod = new AliPayMethod(
                    alipayConfig.getString("name"),
                    alipayConfig.getStringList("cmd-param"),
                    alipayConfig.getString("app-id"),
                    alipayConfig.getString("alipay-public-key"),
                    alipayConfig.getString("merchant-private-key")
            );
            childInjector.injectMembers(payMethod);
            registerPayMethod(payMethod);
            httpServer.addHandler(payMethod.getNotifyPrefix(), payMethod.createHandler(injector));
        }
        Configuration wechatConfig = config.getConfiguration("wechat");
        if (wechatConfig.getBoolean("enable")) {
            PayMethod payMethod = new WeChatMethod(
                    wechatConfig.getString("name"),
                    wechatConfig.getStringList("cmd-param"),
                    wechatConfig.getString("app-id"),
                    wechatConfig.getString("merchant-id"),
                    wechatConfig.getString("merchant-key")
            );
            childInjector.injectMembers(payMethod);
            registerPayMethod(payMethod);
            httpServer.addHandler(payMethod.getNotifyPrefix(), payMethod.createHandler(injector));
        }
        Configuration tenpayConfig = config.getConfiguration("tenpay");
        if (tenpayConfig.getBoolean("enable")) {
            String appId = tenpayConfig.getString("app-id");
            PayMethod payMethod = new TenPayMethod(
                    tenpayConfig.getString("name"),
                    tenpayConfig.getStringList("cmd-param"),
                    appId.isBlank() ? null : appId,
                    tenpayConfig.getString("merchant-id"),
                    tenpayConfig.getString("merchant-key")
            );
            childInjector.injectMembers(payMethod);
            registerPayMethod(payMethod);
            httpServer.addHandler(payMethod.getNotifyPrefix(), payMethod.createHandler(injector));
        }
        Configuration unionPayConfig = config.getConfiguration("union-pay");
        if (unionPayConfig.getBoolean("enable")) {
            String appId = unionPayConfig.getString("app-id");
            PayMethod payMethod = new UnionPayMethod(
                    unionPayConfig.getString("name"),
                    unionPayConfig.getStringList("cmd-param"),
                    appId.isBlank() ? null : appId,
                    unionPayConfig.getString("merchant-id"),
                    unionPayConfig.getString("merchant-key")
            );
            childInjector.injectMembers(payMethod);
            registerPayMethod(payMethod);
            httpServer.addHandler(payMethod.getNotifyPrefix(), payMethod.createHandler(injector));
        }
    }

    /**
     * 获取一个支付方式
     *
     * @param suggest 建议
     * @return 支付方式
     */
    @NotNull
    public Optional<PayMethod> getPayMethod(@NotNull String suggest) {
        return payMethods.stream().filter(method ->
                method.getSuggest().stream().anyMatch(s -> s.equalsIgnoreCase(suggest))
        ).findAny();
    }

    /**
     * 注册一个支付方式
     *
     * @param payMethod 支付方式
     */
    public void registerPayMethod(@NotNull PayMethod payMethod) {
        for (PayMethod method : payMethods) {
            if (method.getName().equals(payMethod.getName())) {
                throw new IllegalArgumentException("支付方式 " + method.getName() + " 已存在!");
            }
        }
        for (String suggest : payMethod.getSuggest()) {
            Optional<PayMethod> method = getPayMethod(suggest);
            if (method.isPresent()) {
                throw new IllegalArgumentException("支付方式 " + payMethod.getName() + " 的建议与支付方式 " + method.get().getName() + " 存在冲突!");
            }
        }
        payMethods.add(payMethod);
    }

    /**
     * 获取所有的支付方式
     *
     * @return 支付方式
     */
    @NotNull
    public List<PayMethod> getPayMethods() {
        return List.copyOf(payMethods);
    }

    /**
     * 获取支付方式的建议
     *
     * @return 所有的建议
     */
    @NotNull
    public Stream<String> getPayMethodSuggestStream() {
        return payMethods.stream().map(PayMethod::getSuggest).flatMap(Collection::stream);
    }

    /**
     * 获取支付方式的建议
     *
     * @return 所有的建议
     */
    @NotNull
    public List<String> getPayMethodSuggest() {
        return getPayMethodSuggestStream().collect(Collectors.toList());
    }

    public boolean isPaying(@NotNull Player player) {
        return paying.contains(player.getUniqueId());
    }

    /**
     * 发起支付
     *
     * @param player    玩家
     * @param payMethod 支付方式
     * @param money     支付金额
     */
    public void initiatePay(@NotNull Player player, @NotNull PayMethod payMethod, double money) {
        if (isPaying(player)) {
            // 正在支付中
            l10n.sendMessage(player, "pay.order.paying");
            return;
        }
        Optional<ServerConnection> currentServer = player.getCurrentServer();
        if (!ProtocolUtil.isConnectedPlayer(player) || currentServer.isEmpty()) {
            // 这种情况应该极少 无法创建订单
            l10n.sendMessage(player, "pay.order.unable-create");
            return;
        }
        String serverName = currentServer.get().getServerInfo().getName();
        Order order = createOrder(player, serverName, money);
        if (order == null) {
            // 订单创建失败 没办法写入到数据库内
            l10n.sendMessage(player, "pay.order.create-failed");
            return;
        }
        String qrcodeUrl = payMethod.initiateNativePay(order);
        if (qrcodeUrl == null) {
            l10n.sendMessage(player, "pay.order.merchant-error");
            return;
        }
        BufferedImage qrCode = FastQrCodeUtil.createQrCode(qrcodeUrl);
        // 发送数据包展示物品
        eventManager.fire(new QrCodeCreateEvent(qrcodeUrl, qrCode)).thenAccept(event -> {
            Optional<BufferedImage> image = event.getResult().getBufferedImage();
            // 其实应该先发 MapView 包的 但是有BUG 全是异步的
            packetManager.sendMapItemPacket(player, order, payMethod);
            packetManager.sendMapViewPacket(player, image.orElse(event.getImage()));
            if (payConfig.isHeadDown()) {
                packetManager.sendHeadDownPacket(player);
            }
        });
        l10n.sendMessage(player, "pay.order.start");
        // 添加到正在支付列表
        paying.add(player.getUniqueId());
    }

    /**
     * 关闭支付
     * 并发送数据包收回物品
     *
     * @param player 玩家
     */
    public void closePay(@NotNull Player player) {
        closePay(player, ClosePayReason.ACTIVE);
    }

    /*
     * 关闭支付
     *
     * @param player 玩家
     * @param active 主动关闭(由玩家主动关闭 只发送关闭信息)
     */
    public void closePay(@NotNull Player player, ClosePayReason reason) {
        if (paying.remove(player.getUniqueId())) {
            // 如果自动关闭要发送数据包更新玩家背包 主动关闭则不需要
            if (reason == ClosePayReason.AUTO && ProtocolUtil.isConnectedPlayer(player)) {
                packetManager.sendDestroyItemPacket(player);
            }
            if (reason != ClosePayReason.COMPLETE) {
                l10n.sendMessage(player, "pay.order.close");
            }
        }
    }

    private void closePay0() {

    }

    /**
     * 标记付款
     *
     * @param orderInfo 订单信息
     */
    public void markPay(Map<String, String> orderInfo) {
        String outTradeNo = orderInfo.get("out_trade_no");
        long orderId = Long.parseLong(outTradeNo);
        if (orderDao.markPay(orderId, Constants.GSON.toJson(orderInfo))) {
            logger.info("订单号 {} 已被支付!", outTradeNo);
            Order order = orderDao.selectById(orderId);
            if (order == null) {
                logger.warn("数据库中无法找到订单号: {}", orderId);
                return;
            }
            eventManager.fireAndForget(new OrderPaymentEvent(order));
            if (shipConfig.isEnable() && shipConfig.isOnPay()) {
                ship(order);
            } {
                // 如果有订单被支付 但未发货 则将标记为true
                hasUnshipOrder = true;
            }
            // 关闭支付
            server.getPlayer(UUID.fromString(order.getBuyer())).ifPresent(this::closePay);
        }
    }

    /**
     * 尝试发货
     *
     * @param player 玩家
     */
    public void tryShip(Player player) {
        // 先判断标记 是否存在未发货订单 减少数据库操作 优化性能
        if (hasUnshipOrder) {
            List<Order> orders = orderDao.selectUnshipOrder(player.getUniqueId().toString());
            orders.forEach(this::ship);
            hasUnshipOrder = false;
        }
    }

    /**
     * 为一个订单发货
     *
     * @param order 订单号
     */
    public void ship(Order order) {
        String buyer = order.getBuyer();
        UUID uuid = UUID.fromString(buyer);
        Optional<Player> player = server.getPlayer(uuid);
        String name;
        if (player.isPresent()) {
            name = player.get().getUsername();
        } else if (!shipConfig.isNeedOnline()) {
            name = playerInfoDao.selectNameByUuid(buyer);
            if (name == null) {
                throw new IllegalStateException("无法找到玩家信息!");
            }
        } else {
            return;
        }

        // 标记发货 并发货
        if (orderDao.markShip(order.getId())) {
            eventManager.fire(new OrderShipEvent(uuid, name, order)).thenAccept(event -> {
                List<String> commands = shipConfig.getCommands();
                for (String command : commands) {
                    server.getCommandManager().executeAsync(server.getConsoleCommandSource(), command.replace("%uuid%", buyer)
                            .replace("%player%", name)
                            .replace("%server%", order.getServer())
                            .replace("%money%", Double.toString(order.getTotalFee()))
                            .replace("%money-int%", Integer.toString((int) order.getTotalFee())));
                }
            });
        }
    }

    /**
     * 创建订单
     *
     * @param player 玩家
     * @param server 服务器
     * @param money  金额
     * @return 订单
     */
    @Nullable
    private Order createOrder(@NotNull Player player, @NotNull String server, double money) {
        Order order = new Order();
        order.setId(sequence.nextId());
        order.setServer(server);
        order.setBuyer(player.getUniqueId().toString());
        order.setSubject(l10n.parse("pay.order.subject", Map.of(
                "%player%", player.getUsername(),
                "%server%", server,
                "%money%", money
        )));
        order.setTotalFee(money);
        order.setStatus(0);
        order.setShip(0);
        order.setCreateTime(new Timestamp(System.currentTimeMillis()));
        return orderDao.insert(order) ? order : null;
    }


    public enum ClosePayReason {

        /**
         * 玩家主动关闭
         */
        ACTIVE,
        /**
         * 自动关闭
         */
        AUTO,
        /**
         * 支付成功后关闭(不发送关闭信息)
         */
        COMPLETE
    }
}
