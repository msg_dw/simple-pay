package me.xpyex.simplepay.pay.method;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import me.xpyex.simplepay.common.Constants;
import me.xpyex.simplepay.domain.Order;
import me.xpyex.simplepay.http.HttpHandler;
import me.xpyex.simplepay.http.handler.UnionPayHandler;
import me.xpyex.simplepay.util.HttpUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;

/**
 * 银联支付 非常方便 云闪付/银行APP/微信/支付宝 都可以用这一个码扫
 * 这玩意tm的就是抄的QQ的 连APPID都是用的微信的
 */
public class UnionPayMethod extends TencentPayMethod {

    public UnionPayMethod(@NotNull String name, @NotNull List<String> suggest, @Nullable String appId, @NotNull String merchantId, @NotNull String merchantKey) {
        super(name, suggest, appId, merchantId, merchantKey);
    }

    @Override
    public String getUnifiedOrderUrl() {
        return "https://qra.95516.com/pay/gateway";
    }

    @Override
    @NotNull
    public String getNotifyPrefix() {
        return Constants.UNION_PAY_NOTIFY_PREFIX;
    }

    @Override
    @NotNull
    public HttpHandler createHandler(Injector injector) {
        Injector childInjector = injector.createChildInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(String.class).annotatedWith(Names.named("MerchantKey")).toInstance(merchantKey);
            }
        });
        return childInjector.getInstance(UnionPayHandler.class);
    }

    @Nullable
    @Override
    public String initiateNativePay(@NotNull Order order) {
        // 参数要排序 这里手动排序用个有序的Map
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("body", order.getSubject());
        params.put("fee_type", "CNY");
        params.put("mch_create_ip", "127.0.0.1");
        params.put("mch_id", merchantId);
        params.put("nonce_str", UUID.randomUUID().toString().replace("-", ""));
        params.put("notify_url", notifyUrl + getNotifyPrefix());
        params.put("out_trade_no", order.getId());
        params.put("service", "unified.trade.native");
        params.put("sign_type", "MD5");
        // 这里最好是传入玩家IP 但是挺麻烦的其实
        params.put("spbill_create_ip", "127.0.0.1");
        if (appId != null) {
            params.put("sub_appid", appId);
        }
        params.put("total_fee", (int) (order.getTotalFee() * 100));
        params.put("trade_type", "NATIVE");

        // 拼接参数
        String paramsString = String.join("&", params.entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue())
                .toArray(String[]::new)
        );
        // 参数最后要放上 商户密钥 进行md5加密 得到签名
        String sign = md5(paramsString + "&key=" + merchantKey);
        // 最后把签名也插入进去
        params.put("sign", sign);

        String body = HttpUtil.post(getUnifiedOrderUrl(), mapToXml(params));

        Matcher matcher = COMPILE.matcher(body);
        if (matcher.find()) {
            String s = matcher.group();
            return s.substring(19, s.length() - 14);
        }
        return null;
    }
}
