package me.xpyex.simplepay.pay.method;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import me.xpyex.simplepay.common.Constants;
import me.xpyex.simplepay.http.HttpHandler;
import me.xpyex.simplepay.http.handler.TencentPayHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * QQ钱包支付方式
 */
public class TenPayMethod extends TencentPayMethod {

    public TenPayMethod(@NotNull String name, @NotNull List<String> suggest, @Nullable String appId, @NotNull String merchantId, @NotNull String merchantKey) {
        super(name, suggest, appId, merchantId, merchantKey);
    }

    @Override
    public String getUnifiedOrderUrl() {
        return "https://qpay.qq.com/cgi-bin/pay/qpay_unified_order.cgi";
    }

    @Override
    @NotNull
    public String getNotifyPrefix() {
        return Constants.TENPAY_NOTIFY_PREFIX;
    }

    @Override
    @NotNull
    public HttpHandler createHandler(Injector injector) {// 添加HTTP处理器 处理异步回调
        Injector childInjector = injector.createChildInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(String.class).annotatedWith(Names.named("MerchantKey")).toInstance(merchantKey);
            }
        });
        return childInjector.getInstance(TencentPayHandler.class);
    }
}
