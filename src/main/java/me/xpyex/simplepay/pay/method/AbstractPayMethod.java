package me.xpyex.simplepay.pay.method;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.xpyex.simplepay.pay.PayMethod;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@Getter
@AllArgsConstructor
public abstract class AbstractPayMethod implements PayMethod {

    @NotNull
    protected final String name;

    @NotNull
    protected final List<String> suggest;
}
