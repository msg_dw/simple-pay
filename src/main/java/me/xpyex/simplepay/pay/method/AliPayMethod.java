package me.xpyex.simplepay.pay.method;

import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import me.xpyex.simplepay.common.Constants;
import me.xpyex.simplepay.http.HttpHandler;
import me.xpyex.simplepay.http.handler.AliPayHandler;
import me.xpyex.simplepay.domain.Order;
import me.xpyex.simplepay.util.HttpUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.*;

/**
 * 支付宝的支付方式
 */
public class AliPayMethod extends AbstractPayMethod {

    @Inject
    @Named("NotifyUrl")
    private String notifyUrl;

    /**
     * 支付宝网关
     */
    private static final String ALIPAY_GATEWAY = "https://openapi.alipay.com/gateway.do";

    /**
     * 支付宝开放平台中创建应用的APPID
     */
    private final String appId;

    /**
     * 支付宝商户平台中交换商户公钥即可获得支付宝公钥
     */
    private final String alipayPublicKey;

    /**
     * 商户私钥 与自己创建的商户公钥对应的商户私钥
     */
    private final String merchantPrivateKey;

    public AliPayMethod(String name, List<String> suggest, String appId, String alipayPublicKey, String merchantPrivateKey) {
        super(name, suggest);
        this.appId = appId;
        this.alipayPublicKey = alipayPublicKey;
        this.merchantPrivateKey = merchantPrivateKey;
    }

    @Override
    @NotNull
    public String getNotifyPrefix() {
        return Constants.ALIPAY_NOTIFY_PREFIX;
    }

    @Override
    public @NotNull HttpHandler createHandler(Injector injector) {
        Injector childInjector = injector.createChildInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(String.class).annotatedWith(Names.named("PublicKey")).toInstance(alipayPublicKey);
            }
        });
        return childInjector.getInstance(AliPayHandler.class);
    }

    @Nullable
    @Override
    public String initiateNativePay(@NotNull Order order) {
        String time = Constants.DATE_FORMAT.format(System.currentTimeMillis());

        Map<String, Object> bizContent = new HashMap<>(3);
        bizContent.put("out_trade_no", order.getId());
        bizContent.put("total_amount", order.getTotalFee());
        bizContent.put("subject", order.getSubject());

        // 参数要排序 这里手动排序用个有序的Map
        Map<String, String> params = new LinkedHashMap<>();
        params.put("app_id", appId);
        params.put("biz_content", Constants.GSON.toJson(bizContent));
        params.put("charset", "UTF-8");
        params.put("method", "alipay.trade.precreate");
        params.put("notify_url", notifyUrl + Constants.ALIPAY_NOTIFY_PREFIX);
        params.put("sign_type", "RSA2");
        params.put("timestamp", time);
        params.put("version", "1.0");

        // 拼接参数
        String paramsString = String.join("&", params.entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue())
                .toArray(String[]::new)
        );

        String sign = rsaSign(paramsString, merchantPrivateKey);
        params.put("sign", sign);

        String join = String.join("&", params.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(e -> e.getKey() + "=" + URLEncoder.encode(e.getValue(), StandardCharsets.UTF_8))
                .toArray(String[]::new)
        );

        String body = HttpUtil.post(ALIPAY_GATEWAY + "?" + join);
        try {
            JsonObject jsonObject = Constants.GSON.fromJson(body, JsonObject.class);
            JsonObject response = jsonObject.getAsJsonObject("alipay_trade_precreate_response");
            return response.get("qr_code").getAsString();
        } catch (JsonSyntaxException | NullPointerException e) {
            return null;
        }
    }

    /**
     * RSA2签名算法
     *
     * @param content    内容
     * @param privateKey 密钥
     * @return 密文
     */
    @NotNull
    public static String rsaSign(@NotNull String content, @NotNull String privateKey) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] encodedKey = Base64.getDecoder().decode(privateKey.getBytes(StandardCharsets.UTF_8));
            PrivateKey priKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(encodedKey));
            Signature signature = Signature.getInstance("SHA256WithRSA");
            signature.initSign(priKey);
            signature.update(content.getBytes(StandardCharsets.UTF_8));
            byte[] signed = signature.sign();
            return Base64.getEncoder().encodeToString(signed);
        } catch (NoSuchAlgorithmException e) {
            throw new UnsupportedOperationException("不支持的加密方式", e);
        } catch (InvalidKeySpecException | InvalidKeyException e) {
            throw new IllegalArgumentException("无效的密钥", e);
        } catch (SignatureException e) {
            throw new IllegalArgumentException("签名异常", e);
        }
    }
}
