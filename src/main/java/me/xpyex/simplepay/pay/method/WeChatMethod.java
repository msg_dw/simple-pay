package me.xpyex.simplepay.pay.method;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import me.xpyex.simplepay.common.Constants;
import me.xpyex.simplepay.http.HttpHandler;
import me.xpyex.simplepay.http.handler.TencentPayHandler;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * 微信支付方式
 */
public class WeChatMethod extends TencentPayMethod {

    public WeChatMethod(@NotNull String name, @NotNull List<String> suggest, @NotNull String appId, @NotNull String merchantId, @NotNull String merchantKey) {
        super(name, suggest, appId, merchantId, merchantKey);
    }

    @Override
    public String getUnifiedOrderUrl() {
        return "https://api.mch.weixin.qq.com/pay/unifiedorder";
    }

    @Override
    @NotNull
    public String getNotifyPrefix() {
        return Constants.WECHAT_NOTIFY_PREFIX;
    }

    @Override
    @NotNull
    public HttpHandler createHandler(Injector injector) {
        Injector childInjector = injector.createChildInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(String.class).annotatedWith(Names.named("MerchantKey")).toInstance(merchantKey);
            }
        });
        return childInjector.getInstance(TencentPayHandler.class);
    }
}
