package me.xpyex.simplepay.pay.method;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import me.xpyex.simplepay.domain.Order;
import me.xpyex.simplepay.util.HttpUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 腾讯系的支付方式
 * 微信和QQ钱包其实是一样的
 * 主要就是请求的URL地址不同 QQ钱包可以省略appId
 * 虽然微信已经了支持JSON报文和HMACSHA256加密
 * 但是QQ使用的XML和MD5 所以统一 还是使用XML和MD5
 */
public abstract class TencentPayMethod extends AbstractPayMethod {

    protected static final Pattern COMPILE = Pattern.compile("<code_url>.*</code_url>");

    @Inject
    @Named("NotifyUrl")
    protected String notifyUrl;

    /**
     * 微信公众平台中创建的公众号/小程序的APPID
     * QQ钱包不需要
     */
    protected final String appId;

    /**
     * 微信/QQ钱包商户平台中的商户号
     */
    protected final String merchantId;

    /**
     * 在微信/QQ钱包商户平台由自己上传的随机32位私钥
     */
    protected final String merchantKey;

    protected TencentPayMethod(@NotNull String name, @NotNull List<String> suggest, @Nullable String appId, @NotNull String merchantId, @NotNull String merchantKey) {
        super(name, suggest);
        this.appId = appId;
        this.merchantId = merchantId;
        this.merchantKey = merchantKey;
    }

    /**
     * 获取统一下单接口Url
     *
     * @return 统一下单接口Url
     */
    abstract String getUnifiedOrderUrl();

    @Nullable
    @Override
    public String initiateNativePay(@NotNull Order order) {
        // 参数要排序 这里手动排序用个有序的Map
        Map<String, Object> params = new LinkedHashMap<>();
        if (appId != null) {
            params.put("appid", appId);
        }
        params.put("body", order.getSubject());
        params.put("fee_type", "CNY");
        params.put("mch_id", merchantId);
        params.put("nonce_str", UUID.randomUUID().toString().replace("-", ""));
        params.put("notify_url", notifyUrl + getNotifyPrefix());
        params.put("out_trade_no", order.getId());
        // 这里最好是传入玩家IP 但是挺麻烦的其实
        params.put("spbill_create_ip", "127.0.0.1");
        params.put("total_fee", (int) (order.getTotalFee() * 100));
        params.put("trade_type", "NATIVE");

        // 拼接参数
        String paramsString = String.join("&", params.entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue())
                .toArray(String[]::new)
        );
        // 参数最后要放上 商户密钥 进行md5加密 得到签名
        String sign = md5(paramsString + "&key=" + merchantKey);
        // 最后把签名也插入进去
        params.put("sign", sign);

        String body = HttpUtil.post(getUnifiedOrderUrl(), mapToXml(params));

        Matcher matcher = COMPILE.matcher(body);
        if (matcher.find()) {
            String s = matcher.group();
            return s.substring(19, s.length() - 14);
        }
        return null;
    }

    /**
     * MD5加密
     *
     * @param content 内容
     * @return 密文
     */
    public static String md5(String content) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("md5");
            byte[] digest = md5.digest(content.getBytes(StandardCharsets.UTF_8));
            return new BigInteger(1, digest).toString(16).toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            throw new UnsupportedOperationException("不支持的加密方式", e);
        }
    }

    /**
     * 将MAP转成XML
     *
     * @return XML字符串
     */
    public static String mapToXml(Map<String, Object> map) {
        // 简单的拼接一下字符串即可
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("<xml>");
        map.forEach((k, v) -> stringBuilder
                .append('<').append(k).append('>')
                .append(v)
                .append("</").append(k).append('>')
        );
        stringBuilder.append("</xml>");

        return stringBuilder.toString();
    }
}
