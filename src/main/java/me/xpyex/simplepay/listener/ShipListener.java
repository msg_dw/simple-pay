package me.xpyex.simplepay.listener;

import com.google.inject.Inject;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.LoginEvent;
import com.velocitypowered.api.event.player.ServerConnectedEvent;
import com.velocitypowered.api.event.player.ServerPostConnectEvent;
import me.xpyex.simplepay.config.SimplePayConfig.ShipConfig;
import me.xpyex.simplepay.pay.PayManager;

/**
 * 发货监听器 为一些情况处理发货
 */
public class ShipListener {

    @Inject
    private PayManager payManager;

    @Inject
    private ShipConfig shipConfig;

    @Subscribe
    public void onJoin(LoginEvent event) {
        // 加入游戏时处理发货
        if (shipConfig.isEnable() && shipConfig.isOnJoin()) {
            payManager.tryShip(event.getPlayer());
        }
    }

    @Subscribe
    public void onServer(ServerConnectedEvent event) {
        // 服务器连接完成时处理发货
        if (shipConfig.isEnable() && shipConfig.isOnServer()) {
            payManager.tryShip(event.getPlayer());
        }
    }

    @Subscribe
    @SuppressWarnings("all")
    public void onServer(ServerPostConnectEvent event) {
        // 服务器连接完成时处理发货
        if (shipConfig.isEnable() && shipConfig.isOnServerFinish()) {
            payManager.tryShip(event.getPlayer());
        }
    }
}
