package me.xpyex.simplepay.listener;

import com.google.inject.Inject;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.DisconnectEvent;
import com.velocitypowered.api.event.connection.LoginEvent;
import com.velocitypowered.api.network.ProtocolVersion;
import com.velocitypowered.api.proxy.Player;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import me.xpyex.simplepay.packet.PacketManager;
import me.xpyex.simplepay.packet.client.SetSlotPacket;
import me.xpyex.simplepay.packet.client.WindowItemsPacket;
import me.xpyex.simplepay.packet.server.PlayerDiggingPacket;
import me.xpyex.simplepay.packet.server.WindowClickPacket;
import me.xpyex.simplepay.pay.PayManager;
import me.xpyex.simplepay.util.ProtocolUtil;
import org.jetbrains.annotations.NotNull;

import java.util.NoSuchElementException;

/**
 * 数据包监听器
 */
public class PacketListener {

    private static final String PACKET_HANDLER = "simplepay-packet-listener";

    @Inject
    private PacketManager packetManager;

    @Inject
    private PayManager payManager;

    @Subscribe
    public void onJoin(LoginEvent event) {
        Player player = event.getPlayer();
        if (ProtocolUtil.isConnectedPlayer(player)) {
            Channel channel = ProtocolUtil.getChannel(player);
            channel.pipeline().addBefore("handler", PACKET_HANDLER,
                    new ChannelDuplexHandler() {
                        @Override
                        public void channelRead(@NotNull ChannelHandlerContext ctx, @NotNull Object msg) throws Exception {
                            if (msg instanceof ByteBuf && onReceivePacket(player, (ByteBuf) msg)) {
                                return;
                            }
                            super.channelRead(ctx, msg);
                        }

                        @Override
                        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
                            if (msg instanceof ByteBuf && onSendPacket(player, (ByteBuf) msg)) {
                                return;
                            }
                            super.write(ctx, msg, promise);
                        }
                    });
        }
    }

    @Subscribe
    public void onQuit(DisconnectEvent event) {
        Player player = event.getPlayer();
        if (ProtocolUtil.isConnectedPlayer(player)) {
            Channel channel = ProtocolUtil.getChannel(player);
            try {
                channel.pipeline().remove(PACKET_HANDLER);
            } catch (NoSuchElementException e) {
                //
            }
        }
    }

    // 没注册包事件 主要是因为 注册事件 也不应该监听自己的事件处理 到时候会更麻烦

    /**
     * 监听发送至客户端的数据包
     *
     * @param player 玩家
     * @param buf    数据包
     * @return 是否拦截数据包
     */
    public boolean onSendPacket(Player player, ByteBuf buf) {
        // 如果在支付中 服务器向玩家发送了 windowItems(更新背包物品)包或setSlot(设置插槽物品)数据包则关闭支付
        if (payManager.isPaying(player)) {
            int ridx = buf.readerIndex();
            int packetId = ProtocolUtil.readVarInt(buf);

            if (packetId == packetManager.getPacketId(SetSlotPacket.class, player.getProtocolVersion())) {
                byte windowId = buf.readByte();
                // 如果是 设置插槽物品 要求是在背包内的第40号插槽
                if (windowId == 0) {
                    if (player.getProtocolVersion().compareTo(ProtocolVersion.MINECRAFT_1_17_1) >= 0) {
                        ProtocolUtil.readVarInt(buf);
                    }
                    short i = buf.readShort();
                    if (i == 40) {
                        payManager.closePay(player, PayManager.ClosePayReason.AUTO);
                    }
                }
                // 另外一种情况是 -1 也要关闭支付
                if (windowId == -1) {
                    payManager.closePay(player);
                }
            }
            // 打开GUI 或 各种更新背包物品的操作 都关闭支付
            else if (packetId == packetManager.getPacketId(WindowItemsPacket.class, player.getProtocolVersion())) {
                payManager.closePay(player);
            }
            buf.readerIndex(ridx);
        }
        return false;
    }

    /**
     * 监听从客户端接收到的数据包
     *
     * @param player 玩家
     * @param buf    数据包
     * @return 是否拦截数据包
     */
    public boolean onReceivePacket(Player player, ByteBuf buf) {
        // 如果在支付中玩家要丢弃物品 拦截这个事件 并关闭支付
        if (payManager.isPaying(player)) {
            int ridx = buf.readerIndex();
            int packetId = ProtocolUtil.readVarInt(buf);

            // 打开背包按Q键丢弃的数据包
            if (packetId == packetManager.getPacketId(WindowClickPacket.class, player.getProtocolVersion()) && buf.readByte() == 0) {
                if (player.getProtocolVersion().compareTo(ProtocolVersion.MINECRAFT_1_17) > 0) {
                    ProtocolUtil.readVarInt(buf);
                }
                // 要求是丢弃40号插槽
                if (buf.readShort() == 40) {
                    buf.readByte();
                    if (player.getProtocolVersion().compareTo(ProtocolVersion.MINECRAFT_1_17) < 0) {
                        buf.readShort();
                    }
                    int mode;
                    if (player.getProtocolVersion().compareTo(ProtocolVersion.MINECRAFT_1_9) >= 0) {
                        mode = ProtocolUtil.readVarInt(buf);
                    } else {
                        mode = buf.readByte();
                    }
                    // 丢弃物品的模式
                    if (mode == 4) {
                        payManager.closePay(player);
                        return true;
                    }
                }
            }
            // 直接丢弃物品 不打开背包
            else if (packetId == packetManager.getPacketId(PlayerDiggingPacket.class, player.getProtocolVersion())) {
                int status;
                if (player.getProtocolVersion().compareTo(ProtocolVersion.MINECRAFT_1_8) <= 0) {
                    status = buf.readByte();
                } else {
                    status = ProtocolUtil.readVarInt(buf);
                }
                // 3 是 Ctrl+Q, 4 是 Q 都是直接丢弃主手的物品
                if (status == 3 || status == 4) {
                    payManager.closePay(player);
                    return true;
                }
            }
            buf.readerIndex(ridx);
        }
        return false;
    }
}
