package me.xpyex.simplepay.listener;

import com.google.inject.Inject;
import com.velocitypowered.api.event.EventTask;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.LoginEvent;
import com.velocitypowered.api.proxy.Player;
import me.xpyex.simplepay.dao.PlayerInfoDao;

/**
 * 玩家监听器
 */
public class PlayerListener {

    @Inject
    private PlayerInfoDao playerInfoDao;

    @Subscribe
    public EventTask onJoin(LoginEvent event) {
        // 将玩家的UUID和姓名 存储到数据库中
        return EventTask.async(() -> {
            Player player = event.getPlayer();
            playerInfoDao.insertOrUpdate(player.getUniqueId(), player.getUsername());
        });
    }
}
