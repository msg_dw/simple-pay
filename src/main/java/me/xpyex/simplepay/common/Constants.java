package me.xpyex.simplepay.common;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;

/**
 * 常量
 */
public interface Constants {

    /**
     * 日期格式
     */
    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 一个默认的GSON对象
     */
    Gson GSON = new Gson();

    /**
     * 支付宝异步通知URL前缀
     */
    String ALIPAY_NOTIFY_PREFIX = "/alipay";

    /**
     * 微信异步通知URL前缀
     */
    String WECHAT_NOTIFY_PREFIX = "/wechat";

    /**
     * QQ钱包异步通知URL前缀
     */
    String TENPAY_NOTIFY_PREFIX = "/tenpay";

    /**
     * 中国银联异步通知URL前缀
     */
    String UNION_PAY_NOTIFY_PREFIX = "/union-pay";

}
